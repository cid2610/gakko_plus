$(function () {
    'use strict';
    // process upload image
    $('#imageUpload').fileupload({
        maxNumberOfFiles: 1,
        dataType: 'json',
        done: function (e, data) {
            console.log(data);
            $.each(data.files, function (index, file) {
                $('#display_name').html(file.name);
            });
            $('#image').val(data.result.name);
        },
        progressall: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('#progress .progress-bar').css(
                'width',
                progress + '%'
            );
        }
    }).prop('disabled', !$.support.fileInput).parent().addClass($.support.fileInput ? undefined : 'disabled');
});
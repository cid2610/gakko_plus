var videoHeight = 500;
if ($(window).width() < 750) {
    videoHeight = $(window).width() * 11 / 16;
}
document.getElementById("videoContainer").style.height = videoHeight + "px";
var skinClass;
switch (window.theme) {
    case "amp-flush":
        skinClass = "amp-flush-skin";
        break;
    case "stream":
        skinClass = "amp-stream-skin";
        break;
    default:
        skinClass = "amp-default-skin";
}

$("#videoContainer").append('<video id="azuremediaplayer" class="azuremediaplayer amp-big-play-centered ' + skinClass + '" width="100%" height="100%" tabindex=0><p class="amp-no-js">To view this video please enable JavaScript, and consider upgrading to a web browser that supports HTML5 video</p></video>');
$(document).ready(function () {
    window.addEventListener('resize', function (event) {
        if (window.innerWidth < 750) {
            document.getElementById("videoContainer").style.height = $(window).width() * 11 / 16 + "px";
        } else {
            document.getElementById("videoContainer").style.height = "500px";
        }
    });
});
var myPlayer = amp('azuremediaplayer', { /* Options */
        "nativeControlsForTouch": false,
        autoplay: true,
        controls: true,
    }, function() {
        console.log('Good to go!');
        // add an event listener
        this.addEventListener('ended', function() {
            console.log('Finished!');
        })
    }
);
var streamUrl = $('#videoContainer').data('url');
console.log(streamUrl);
if(streamUrl){
    myPlayer.src([{
        src: streamUrl,
        //type: "application/vnd.ms-sstr+xml"
    }]);
}

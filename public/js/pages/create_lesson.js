$(function () {
    'use strict';
    // process upload image
    $('#uploadMovie').fileupload({
        maxNumberOfFiles: 1,
        dataType: 'json',
        beforeSend: function () {
            $('#status').text('アップロード中...')
        },
        // Upload success
        done: function (e, data) {
            console.log(data);
            $('#asset_id').val(data.result.asset_id);
            $('#status').text('アップロード完了');
        },
        // Upload fail
        fail: function (e, data) {
            $('#status').text('アップロードエラー発生しました。再度アップロードしてください！');
            $('#progress .progress-bar').css(
                'width',
                '0%'
            );
        },
        progressall: function (e, data) {
            var progress = parseInt(data.loaded / data.total * 100, 10);
            $('#progress .progress-bar').css(
                'width',
                progress + '%'
            );
            $('#status').text('サービス処理中...')
        }
    }).prop('disabled', !$.support.fileInput).parent().addClass($.support.fileInput ? undefined : 'disabled');

    // Create thumbnail
    $('#uploadMovie').on('change', function () {
        var canvas = document.getElementById('canvas');
        var video = document.getElementById('video');
        var capture = document.getElementById('capture');
        video.src = URL.createObjectURL(this.files[0]);
        video.load();
        video.addEventListener('loadeddata', function () {
            if (canvas.getContext) {
                if(window.navigator.msSaveBlob)
                {// IEなら独自関数を使います。
                    console.log(video);
                    canvas.getContext('2d').drawImage(video, 0, 0, 1080, 1920 );
                } else{
                    var ctx = canvas.getContext("2d");   // Get the context.
                    ctx.clearRect(0, 0, canvas.width, canvas.height);    // Clear the last image, if it exists.
                    ctx.drawImage(video, 1, 1);
                    ctx.drawImage(video, 0, 0, 512, 384 );
                }
            }
            // canvas.getContext('2d').drawImage(video, 0, 0, 200, 200 * video.videoHeight/ video.videoWidth );
        });
        capture.addEventListener('click',function(){
            canvas.getContext('2d').drawImage(video, 0, 0, 512, 384 );
        });
    });



    $('form').on('submit', function () {
//        var uploadFile = $('#imageupload').val();
//
//        if(uploadFile){
//            // Nothing
//        }else{
        var canvas = document.getElementById('canvas');
        $('#thumbnail').val(canvas.toDataURL('image/jpeg'));
//        }
    });


});




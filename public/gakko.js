$(function() {
    $('.center-item').slick({
        infinite: true,
        dots:true,
        slidesToShow: 1,
        variableWidth: true,
        centerMode: true, //要素を中央寄せ
        centerPadding:'200px', //両サイドの見えている部分のサイズ
        autoplay:true, //自動再生
        responsive: [{
            breakpoint: 768,
            settings: {
                slidesToShow: 1,
                centerMode: true,
                centerPadding:'200px', //両サイドの見えている部分のサイズ
            }
        },{
            breakpoint: 480,
            settings: {
                slidesToShow: 1,
                centerMode: true,
                centerPadding:'100px', //両サイドの見えている部分のサイズ
            }
        }]
    });

    $('.center-item-content_teacher').slick({
        infinite: true,
        slidesToShow: 5,
        arrows: true,
        prevArrow: '<a class="slick-prev" href="#">前へ</a>',
        nextArrow: '<a class="slick-next" href="#">次へ</a>',
        infinite: false, //スライドループ
        initialSlide: 0, //開始スライド
        centerMode: false, //要素を中央寄せ
        centerPadding:'0px', //両サイドの見えている部分のサイズ
        responsive: [{
            breakpoint: 768,
            settings: {
                slidesToShow: 3,
                centerMode: false,
                centerPadding:'200px', //両サイドの見えている部分のサイズ
            }
        },{
            breakpoint: 480,
            settings: {
                slidesToShow: 2,
                centerMode: false,
                centerPadding:'400px', //両サイドの見えている部分のサイズ
            }
        }]
    });

    $('.center-item-content').slick({
        infinite: true,
        slidesToShow: 4,
        arrows: true,
        prevArrow: '<a class="slick-prev" href="#">前へ</a>',
        nextArrow: '<a class="slick-next" href="#">次へ</a>',
        infinite: false, //スライドループ
        initialSlide: 0, //開始スライド
        centerMode: false, //要素を中央寄せ
        centerPadding:'0px', //両サイドの見えている部分のサイズ
        responsive: [{
            breakpoint: 980,
            settings: {
                slidesToShow: 3,
                centerMode: false,
                centerPadding:'200px', //両サイドの見えている部分のサイズ
            }
        },{
            breakpoint: 768,
            settings: {
                slidesToShow: 3,
                centerMode: false,
                centerPadding:'200px', //両サイドの見えている部分のサイズ
            }
        },{
            breakpoint: 480,
            settings: {
                slidesToShow: 2,
                centerMode: false,
                centerPadding:'400px', //両サイドの見えている部分のサイズ
            }
        }]
    });



    $("#create").change(function () {
        var str = "";
        str = $(this).val();

        // 初期化
        $("#course_id").val("");
        $("#classname").val("");
        $("#gaiyou").val("");
        $("#course_imagebox").html("");

        // 有効化・無効化
        if (str == "選択して下さい") {
            $("#classname").prop("disabled", true);
            $("#gaiyou").prop("disabled", true);
            $("#classup").prop("disabled", true);
        } else {
            $("#classname").prop("disabled", false);
            $("#gaiyou").prop("disabled", false);
            $("#classup").prop("disabled", false);
        }

    }).change();


    //チャプター用
    for (var i=1; i<31; i++) {
        $('#chapter'+i).timepicker({
            showMeridian : false
        });
    }
});
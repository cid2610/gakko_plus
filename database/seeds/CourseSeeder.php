<?php

use Illuminate\Database\Seeder;

class CourseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->truncate();

        //
        DB::table('course')->insert([
            'title' => '板金動画',
            'overview' => '板金のノウハウ',
            'image' => 'https://gakkoplus.blob.core.windows.net/course/0021.png',
            'category' => 'category2'
        ]);

        DB::table('course')->insert([
            'title' => 'プログラミング ',
            'overview' => 'HTMLが覚えられる',
            'image' => 'https://gakkoplus.blob.core.windows.net/course/1866.png',
            'category' => 'category2'
        ]);

        DB::table('course')->insert([
            'title' => 'テスト講座',
            'overview' => 'ここに講座概要が表示されます。これは講座概要のテストです。<br>',
            'image' => 'https://gakkoplus.blob.core.windows.net/course/3500.png',
            'category' => 'category0,category1'
        ]);

        DB::table('course')->insert([
            'title' => '動画制作',
            'overview' => '動画制作の基礎に関して指導 ',
            'image' => 'https://gakkoplus.blob.core.windows.net/course/3963.png',
            'category' => 'category0 '
        ]);

        DB::table('course')->insert([
            'title' => 'ダイエット講座① ',
            'overview' => 'ダイエットの講座',
            'image' => 'https://gakkoplus.blob.core.windows.net/course/4939.png',
            'category' => 'category3'
        ]);

        DB::table('course')->insert([
            'title' => 'ビジネス講座',
            'overview' => 'http://www.econtext.jp/bank_list/list.html  ',
            'image' => 'https://gakkoplus.blob.core.windows.net/course/5481.png',
            'category' => 'category0'
        ]);
    }
}

<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //Schema::drop('users');

        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->string('email');
            $table->string('password');
            $table->text('permissions')->nullable();
            $table->timestamp('last_login')->nullable();
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();

            $table->string('username', 50)->nullable();
            $table->string('sex', 5)->nullable();
            $table->date('birthday')->nullable();
            $table->string('location', 100)->nullable();
            $table->string('phone_number', 15)->nullable();
            $table->string('job')->nullable();
            $table->string('mail_option', 3)->nullable();
            $table->string('service_plan', 16)->nullable();
            $table->text('my_message')->nullable();
            $table->string('company', 100)->nullable();
            $table->integer('follow')->nullable();
            $table->integer('follower')->nullable();
            $table->string('link_twitter', 40)->nullable();
            $table->string('link_facebook', 40)->nullable();
            $table->string('link_linkedin', 40)->nullable();
            $table->string('link_google', 40)->nullable();

            $table->timestamps();

            $table->engine = 'InnoDB';
            $table->unique('email');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}

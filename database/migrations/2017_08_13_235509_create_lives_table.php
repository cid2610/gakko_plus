<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLivesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lives', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id');
            $table->integer('course_id')->nullable();
            $table->string('title', 255)->nullable();
            $table->string('image', 255)->nullable();
            $table->dateTime('start_time')->nullable();
            $table->dateTime('end_time')->nullable();
            $table->string('channel_name', 255)->nullable()->unique();
            $table->string('channel_id', 255)->nullable();
            $table->string('program_name', 255)->nullable();
            $table->string('program_id', 255)->nullable();
            $table->string('ingest_url', 255)->nullable();
            $table->string('preview_url', 255)->nullable();
            $table->string('streaming_url', 255)->nullable();
            $table->tinyInteger('publish')->nullable();

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lives');
    }
}

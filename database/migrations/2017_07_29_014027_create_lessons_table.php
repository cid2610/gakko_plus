<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLessonsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lessons', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('course_id')->nullable();
            $table->integer('user_id')->nullable();
            $table->string('thumbnail', 255)->nullable();
            $table->tinyInteger('status')->nullable();
            $table->string('title', 50)->nullable();
            $table->text('chapter_name')->nullable();
            $table->string('chapter_time', 80)->nullable();
            $table->string('old_filename', 80)->nullable();
            $table->string('asset_id')->nullable();
            $table->string('streaming_url')->nullable();
            $table->integer('count');

            $table->timestamps();

            //DB::update('ALTER TABLE lessons AUTO_INCREMENT = 1000;');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('lessons');
    }
}

<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// ===============================================
// ADMIN SECTION =================================
// ===============================================
Route::group(array('prefix' => 'admin'), function()
{
    // main page for the admin section (app/views/admin/dashboard.blade.php)
    Route::get('/', function()
    {
        return View::make('centaur.dashboard');
    });

    // subpage for the posts found at /admin/posts (app/views/admin/posts.blade.php)
    Route::get('posts', function()
    {
        return View::make('admin.posts');
    });

    // subpage to create a post found at /admin/posts/create (app/views/admin/posts-create.blade.php)
    Route::get('posts/create', function()
    {
        return View::make('admin.posts-create');
    });
});

Route::get('/', ['as' => 'top', 'uses' => 'TopController@index']);
Route::get('/error', ['as' => 'top.error', 'uses' => 'TopController@error']);
Route::get('/search', ['as' => 'search', 'uses' => 'TopController@search']);

// Authorization
Route::get('/login', ['as' => 'auth.login.form', 'uses' => 'Auth\SessionController@getLogin']);
Route::post('/login', ['as' => 'auth.login.attempt', 'uses' => 'Auth\SessionController@postLogin']);
Route::get('/logout', ['as' => 'auth.logout', 'uses' => 'Auth\SessionController@getLogout']);

// Registration
Route::get('register', ['as' => 'auth.register.form', 'uses' => 'Auth\RegistrationController@getRegister']);
Route::post('register', ['as' => 'auth.register.attempt', 'uses' => 'Auth\RegistrationController@postRegister']);
Route::get('notice', ['as' => 'notice', 'uses' => function() {
    return view('centaur.notice');
}]);


// Activation
Route::get('activate/{code}', ['as' => 'auth.activation.attempt', 'uses' => 'Auth\RegistrationController@getActivate']);
Route::get('resend', ['as' => 'auth.activation.request', 'uses' => 'Auth\RegistrationController@getResend']);
Route::post('resend', ['as' => 'auth.activation.resend', 'uses' => 'Auth\RegistrationController@postResend']);

// Password Reset
Route::get('password/reset/{code}', ['as' => 'auth.password.reset.form', 'uses' => 'Auth\PasswordController@getReset']);
Route::post('password/reset/{code}', ['as' => 'auth.password.reset.attempt', 'uses' => 'Auth\PasswordController@postReset']);
Route::get('password/reset', ['as' => 'auth.password.request.form', 'uses' => 'Auth\PasswordController@getRequest']);
Route::post('password/reset', ['as' => 'auth.password.request.attempt', 'uses' => 'Auth\PasswordController@postRequest']);


Route::group(['middleware' => ['guest']], function() {
    Route::get('profile/{id}', ['as' => 'profile', 'uses' => 'TopController@getProfile' ]);
    Route::get('editprofile', ['as' => 'editprofile', 'uses' => 'TopController@editProfile' ]);
    Route::post('updateprofile', ['as' => 'updateprofile', 'uses' => 'TopController@updateProfile' ]);
    Route::get('my_lives', ['as' => 'my_lives', 'uses' => 'TopController@myLives' ]);

    // Users
    Route::resource('users', 'UserController');

    // Roles
    Route::resource('roles', 'RoleController');

    Route::get('/lives/publish/{id}', ['as' => 'lives.publish', 'uses' => 'LiveController@publish' ]);
    Route::get('/lives/un_publish/{id}', ['as' => 'lives.un_publish', 'uses' => 'LiveController@unPublish' ]);
    Route::get('/lives/display/{id}', ['as' => 'lives.display', 'uses' => 'LiveController@display' ]);

});


//// Single route
//Route::get('profile', function () {
//    // Only authenticated users may enter...
//})->middleware('auth');


// Dashboard
Route::get('dashboard', ['as' => 'dashboard', 'uses' => function() {
    return view('centaur.dashboard');
}]);

// Lessons
Route::resource('lessons', 'LessonController');

// Courses
Route::resource('courses','CourseController');

// Lives
Route::resource('lives', 'LiveController');

Route::post('/upload/video/{type}',array('as'=>'upload.video', 'before'=>'auth', 'uses'=>'UploadController@video'));
Route::post('/upload/image/{type}',array('as'=>'upload.image', 'before'=>'auth', 'uses'=>'UploadController@image'));


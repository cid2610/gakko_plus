<?php
namespace App\Lib;

require_once '..\vendor\autoload.php';

use MicrosoftAzure\Storage\Blob\Models\Block;
use WindowsAzure\Common\ServicesBuilder;
use MicrosoftAzure\Storage\Blob\Models\CreateContainerOptions;
use MicrosoftAzure\Storage\Blob\Models\PublicAccessType;
use MicrosoftAzure\Storage\Common\ServiceException;
use WindowsAzure\Common\Internal\MediaServicesSettings;
use WindowsAzure\MediaServices\MediaServicesRestProxy;
use WindowsAzure\MediaServices\Models\Asset;
use WindowsAzure\MediaServices\Models\AccessPolicy;
use WindowsAzure\MediaServices\Models\AssetDeliveryPolicy;
use WindowsAzure\MediaServices\Models\AssetDeliveryPolicyType;
use WindowsAzure\MediaServices\Models\AssetDeliveryProtocol;
use WindowsAzure\MediaServices\Models\Locator;
use WindowsAzure\MediaServices\Models\Task;
use WindowsAzure\MediaServices\Models\Job;
use WindowsAzure\MediaServices\Models\TaskOptions;

define("CONNECTION_STRING", "DefaultEndpointsProtocol=https;AccountName=feifeichatbot5xgokk;AccountKey=FdH3yWv44mmRcPdNimPn3T4XtD416XGoPC6JcsCee3NZ1pkPyS1plwXQEWczai4jrIvRDyYAf6l74Y73Gr2k9Q==");
//define("CONNECTION_STRING", "DefaultEndpointsProtocol=https;AccountName=gakkoplus;AccountKey=ZjnoV5QZbeSZmOpw+Qs3+q4Yayd8XXrXm8I+bz9ywp38iDonxNK4WuLTAwsjqCkGmayE5if7EZ6zUna/QcgIww==");
define("BLOCKSIZE", 4 * 1024 * 1024);
define("PADLENGTH", 5);

class AzureStorageBlob {

    ////////////////////
    // Helper methods //
    ////////////////////
    /**
     * @param $con
     * @param $file_path
     * @param $filename
     * @return bool
     */
    public function upload($container_name, $file_path, $filename) {
        define("CONTAINERNAME", $container_name);
        define("FILENAME", $file_path);
        define("BLOCKBLOBNAME", basename($filename));
        try {
            if (null == CONNECTION_STRING || "" == CONNECTION_STRING)
            {
                exit();
            }

            $blobRestProxy = ServicesBuilder::getInstance()->createBlobService(CONNECTION_STRING);

            if (!file_exists(FILENAME))
            {
                exit();
            }

            $handle = fopen(FILENAME, "r");

            // Upload the blob using blocks.
            $counter = 1;
            $blockIds = array();

            while (!feof($handle))
            {
                $blockId = str_pad($counter, PADLENGTH, "0", STR_PAD_LEFT);

                $block = new Block();
                $block->setBlockId(base64_encode($blockId));
                $block->setType("Uncommitted");
                array_push($blockIds, $block);

                $data = fread($handle, BLOCKSIZE);

                // Upload the block.
                $blobRestProxy->createBlobBlock(CONTAINERNAME, BLOCKBLOBNAME, base64_encode($blockId), $data);
                $counter++;
            }

            // Done creating the blocks. Close the file and commit the blocks.
            fclose($handle);

            $blobRestProxy->commitBlobBlocks(CONTAINERNAME, BLOCKBLOBNAME, $blockIds);

        }
        catch(\WindowsAzure\Common\ServiceException $serviceException)
        {
            echo "ServiceException encountered.\n";
            $code = $serviceException->getCode();
            $error_message = $serviceException->getMessage();
            echo "$code: $error_message";
        }
        catch (\Exception $exception)
        {
            echo "Exception encountered.\n";
            $code = $exception->getCode();
            $error_message = $exception->getMessage();
            echo "$code: $error_message";
        }

        return true;
    }



    /**
     * @param MediaServicesRestProxy $restProxy
     * @param $mediaFileName
     * @return Asset
     */
    function uploadFileAndCreateAsset(MediaServicesRestProxy $restProxy, $mediaFileName)
    {
        // Create an empty "Asset" by specifying the name
        $asset = new Asset(Asset::OPTIONS_NONE);
        $asset->setName('Media File ' . basename($mediaFileName));
        $asset = $restProxy->createAsset($asset);
        $assetId = $asset->getId();

        echo "Asset created: name={$asset->getName()} id={$assetId}".PHP_EOL;

        // Create an Access Policy with Write permissions
        $accessPolicy = new AccessPolicy('UploadAccessPolicy');
        $accessPolicy->setDurationInMinutes(60.0);
        $accessPolicy->setPermissions(AccessPolicy::PERMISSIONS_WRITE);
        $accessPolicy = $restProxy->createAccessPolicy($accessPolicy);

        // Create a SAS Locator for the Asset
        $sasLocator = new Locator($asset,  $accessPolicy, Locator::TYPE_SAS);
        $sasLocator->setStartTime(new \DateTime('now -5 minutes'));
        $sasLocator = $restProxy->createLocator($sasLocator);

        // Get the mezzanine file content
        $fileContent = file_get_contents($mediaFileName);

        echo "Uploading...".PHP_EOL;

        // Use the 'uploadAssetFile' to perform a multi-part upload using the Block Blobs REST API storage operations
        $restProxy->uploadAssetFile($sasLocator, basename($mediaFileName), $fileContent);

        // Notify Media Services that the file upload operation is done to generate the asset file metadata
        $restProxy->createFileInfos($asset);

        echo 'File uploaded: size='.strlen($fileContent).PHP_EOL;

        // Delete the SAS Locator (and Access Policy) for the Asset since we are done uploading files
        $restProxy->deleteLocator($sasLocator);
        $restProxy->deleteAccessPolicy($accessPolicy);

        return $asset;
    }


    function encodeToAdaptiveBitrateMP4Set(MediaServicesRestProxy $restProxy, Asset $asset)
    {
        // 2.1 retrieve the latest 'Media Encoder Standard' processor version
        $mediaProcessor = $restProxy->getLatestMediaProcessor('Media Encoder Standard');

        echo "Using Media Processor: {$mediaProcessor->getName()} version {$mediaProcessor->getVersion()}".PHP_EOL;

        // 2.2 Create the Job; this automatically schedules and runs it
        $outputAssetName = 'Encoded '.$asset->getName();
        $outputAssetCreationOption = Asset::OPTIONS_NONE;
        $taskBody = '<?xml version="1.0" encoding="utf-8"?><taskBody><inputAsset>JobInputAsset(0)</inputAsset><outputAsset assetCreationOptions="'.$outputAssetCreationOption.'" assetName="'.$outputAssetName.'">JobOutputAsset(0)</outputAsset></taskBody>';

        $task = new Task($taskBody, $mediaProcessor->getId(), TaskOptions::NONE);
        $task->setConfiguration('H264 Multiple Bitrate 720p');

        $job = new Job();
        $job->setName('Encoding Job');

        $job = $restProxy->createJob($job, array($asset), array($task));

        echo "Created Job with Id: {$job->getId()}".PHP_EOL;

        // 2.3 Check to see if the Job has completed
        $result = $restProxy->getJobStatus($job);

        $jobStatusMap = array('Queued', 'Scheduled', 'Processing', 'Finished', 'Error', 'Canceled', 'Canceling');

        while ($result != Job::STATE_FINISHED && $result != Job::STATE_ERROR && $result != Job::STATE_CANCELED) {
            echo "Job status: {$jobStatusMap[$result]}".PHP_EOL;
            sleep(5);
            $result = $restProxy->getJobStatus($job);
        }

        if ($result != Job::STATE_FINISHED) {
            echo "The job has finished with a wrong status: {$jobStatusMap[$result]}".PHP_EOL;
            exit(-1);
        }

        echo "Job Finished!".PHP_EOL;

        // 2.4 Get output asset
        $outputAssets = $restProxy->getJobOutputMediaAssets($job);
        $encodedAsset = $outputAssets[0];

        echo "Asset encoded: name={$encodedAsset->getName()} id={$encodedAsset->getId()}".PHP_EOL;

        return $encodedAsset;
    }


    function publishEncodedAsset(MediaServicesRestProxy $restProxy, $encodedAsset)
    {
        // 6.1 Get the .ISM AssetFile
        $files = $restProxy->getAssetAssetFileList($encodedAsset);
        $manifestFile = null;

        foreach ($files as $file) {
            if (endsWith(strtolower($file->getName()), '.ism')) {
                $manifestFile = $file;
            }
        }

        if ($manifestFile == null) {
            echo "Unable to found the manifest file".PHP_EOL;
            exit(-1);
        }

        // 6.2 Create a 30-day read-only AccessPolicy
        $access = new AccessPolicy('Streaming Access Policy');
        $access->setDurationInMinutes(60 * 24 * 30);
        $access->setPermissions(AccessPolicy::PERMISSIONS_READ);
        $access = $restProxy->createAccessPolicy($access);

        // 6.3 Create a Locator using the AccessPolicy and Asset
        $locator = new Locator($encodedAsset, $access, Locator::TYPE_ON_DEMAND_ORIGIN);
        $locator->setName('Streaming Locator');
        $locator = $restProxy->createLocator($locator);

        // 6.4 Create a Smooth Streaming base URL
        $stremingUrl = $locator->getPath().$manifestFile->getName().'/manifest';

        echo "Streaming URL: {$stremingUrl}".PHP_EOL;
    }


    function runIndexingJob(MediaServicesRestProxy $restProxy, Asset $asset, $taskConfiguration)
    {
        // Retrieve the latest 'Azure Media Indexer' processor version
        $mediaProcessor = $restProxy->getLatestMediaProcessor('Azure Media Indexer');

        echo "Using Media Processor: {$mediaProcessor->getName()} version {$mediaProcessor->getVersion()}".PHP_EOL;

        // Create the Job; this automatically schedules and runs it
        $outputAssetName = 'Indexer Results '.$asset->getName();
        $outputAssetCreationOption = Asset::OPTIONS_NONE;
        $taskBody = '<?xml version="1.0" encoding="utf-8"?><taskBody><inputAsset>JobInputAsset(0)</inputAsset><outputAsset assetCreationOptions="'.$outputAssetCreationOption.'" assetName="'.$outputAssetName.'">JobOutputAsset(0)</outputAsset></taskBody>';

        $task = new Task($taskBody, $mediaProcessor->getId(), TaskOptions::NONE);
        $task->setName('Indexing Task');
        $task->setConfiguration($taskConfiguration);

        $job = new Job();
        $job->setName('Indexing Job');

        $job = $restProxy->createJob($job, array($asset), array($task));

        echo "Created Job with Id: {$job->getId()}".PHP_EOL;

        // Check to see if the Job has completed
        $result = $restProxy->getJobStatus($job);

        $jobStatusMap = array('Queued', 'Scheduled', 'Processing', 'Finished', 'Error', 'Canceled', 'Canceling');

        while ($result != Job::STATE_FINISHED && $result != Job::STATE_ERROR && $result != Job::STATE_CANCELED) {
            echo "Job status: {$jobStatusMap[$result]}".PHP_EOL;
            sleep(5);
            $result = $restProxy->getJobStatus($job);
        }

        if ($result != Job::STATE_FINISHED) {
            echo "The job has finished with a wrong status: {$jobStatusMap[$result]}".PHP_EOL;
            exit(-1);
        }

        echo "Job Finished!".PHP_EOL;

        // Get output asset
        $outputAssets = $restProxy->getJobOutputMediaAssets($job);
        $outputAsset = $outputAssets[0];

        echo "Output asset: name={$outputAsset->getName()} id={$outputAsset->getId()}".PHP_EOL;

        return $outputAsset;
    }

    function downloadAssetFiles(MediaServicesRestProxy $restProxy, $asset, $destinationPath)
    {
        // Create destination directory if does not exist
        if (!file_exists($destinationPath)) {
            mkdir($destinationPath);
        }

        // Create an Access Policy with Read permissions
        $accessPolicy = new AccessPolicy('DownloadAccessPolicy');
        $accessPolicy->setDurationInMinutes(60.0);
        $accessPolicy->setPermissions(AccessPolicy::PERMISSIONS_READ);
        $accessPolicy = $restProxy->createAccessPolicy($accessPolicy);

        // Create a SAS Locator for the Asset
        $sasLocator = new Locator($asset, $accessPolicy, Locator::TYPE_SAS);
        $sasLocator->setStartTime(new \DateTime('now -5 minutes'));
        $sasLocator = $restProxy->createLocator($sasLocator);

        $files = $restProxy->getAssetAssetFileList($asset);

        // Download Asset Files
        foreach ($files as $file) {
            echo "Downloading {$file->getName()} output file...";
            $restProxy->downloadAssetFile($file, $sasLocator, $destinationPath);
            echo "Done!".PHP_EOL;
        }

        // Clean up Locator and Access Policy
        $restProxy->deleteLocator($sasLocator);
        $restProxy->deleteAccessPolicy($accessPolicy);
    }


    /**
     * @param MediaServicesRestProxy $restProxy
     * @param $options
     * @return Channel
     */
    function createAndStartChannel(MediaServicesRestProxy $restProxy, $options)
    {
        // 1 - prepare the channel template
        $channelData = createChannelData($options);

        // 2 - create the channel
        echo "Creating channel... ";
        $channel = $restProxy->createChannel($channelData);

        // 3 - start the channel
        echo "Done!".PHP_EOL."Starting channel... ";
        $restProxy->startChannel($channel);

        echo "Done!".PHP_EOL;
        // 4 - get and return the started channel
        return $restProxy->getChannel($channel);
    }

    function createAsset(MediaServicesRestProxy $restProxy, $options)
    {
        $result = new stdClass();

        // 1 - prepare the program asset
        $result->asset = new Asset(Asset::OPTIONS_NONE);
        $result->asset->setName($options->programName);
        $result->asset = $restProxy->createAsset($result->asset);

        // 2 - create & apply asset delivey policy + AES encryption
        if ($options->aesDynamicEncription) {
            echo "Applying AES Dynamic Encryption... ";
            $result->generatedTestToken = applyAesDynamicEncryption($restProxy, $result->asset, $options);
            echo "Done!".PHP_EOL;
        } else {
            applyNonDynamicEncription($restProxy, $result->asset);
        }

        return $result;
    }
    function applyNonDynamicEncription(MediaServicesRestProxy  $restProxy, $asset)
    {
        $policy = new AssetDeliveryPolicy();
        $policy->setName("Clear Policy");
        $policy->setAssetDeliveryProtocol(AssetDeliveryProtocol::SMOOTH_STREAMING | AssetDeliveryProtocol::DASH | AssetDeliveryProtocol::HLS);
        $policy->setAssetDeliveryPolicyType(AssetDeliveryPolicyType::NO_DYNAMIC_ENCRYPTION);

        $policy = $restProxy->createAssetDeliveryPolicy($policy);
        $restProxy->linkDeliveryPolicyToAsset($asset, $policy);
    }
    function publishLiveAsset(MediaServicesRestProxy $restProxy, $asset)
    {
        // 1 Get the .ISM AssetFile
        echo "Publishing Asset...";
        $files = $restProxy->getAssetAssetFileList($asset);
        $manifestFile = null;

        foreach ($files as $file) {
            if (endsWith(strtolower($file->getName()), '.ism')) {
                $manifestFile = $file;
            }
        }

        if ($manifestFile == null) {
            echo "Unable to found the manifest file".PHP_EOL;
            exit(-1);
        }

        // 2 Create a 30-day read-only AccessPolicy
        $access = new AccessPolicy('Live Access Policy');
        $access->setDurationInMinutes(60 * 24 * 30);
        $access->setPermissions(AccessPolicy::PERMISSIONS_READ);
        $access = $restProxy->createAccessPolicy($access);

        // 3 Create a Locator using the AccessPolicy and Asset
        $locator = new Locator($asset, $access, Locator::TYPE_ON_DEMAND_ORIGIN);
        $locator->setName('Live Locator');
        $locator = $restProxy->createLocator($locator);

        echo "Done!".PHP_EOL;
        // 4 Return the Smooth Streaming base URL
        return $locator->getPath().$manifestFile->getName().'/manifest';
    }
    /*
    $storage = new AzureStorageBlob();
    $file_path = 'over64mb_file.mp4';
    $storage->upload($file_path);

    $storage = new AzureStorageBlob();
    $file_path =  $_FILES['upfile']['tmp_name'];
    $storage->upload('movie', $file_path, 'TestFile');
    */

}

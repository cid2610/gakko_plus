<?php
namespace App\Lib;

use MicrosoftAzure\Storage\Blob\Models\Block;
use WindowsAzure\Common\Internal\Utilities;
use WindowsAzure\Common\ServicesBuilder;
use MicrosoftAzure\Storage\Blob\Models\CreateContainerOptions;
use MicrosoftAzure\Storage\Blob\Models\PublicAccessType;
use MicrosoftAzure\Storage\Common\ServiceException;
use WindowsAzure\Common\Internal\MediaServicesSettings;
use WindowsAzure\MediaServices\MediaServicesRestProxy;
use WindowsAzure\MediaServices\Models\Asset;
use WindowsAzure\MediaServices\Models\AccessPolicy;
use WindowsAzure\MediaServices\Models\AssetDeliveryPolicy;
use WindowsAzure\MediaServices\Models\AssetDeliveryPolicyConfigurationKey;
use WindowsAzure\MediaServices\Models\AssetDeliveryPolicyType;
use WindowsAzure\MediaServices\Models\AssetDeliveryProtocol;
use WindowsAzure\MediaServices\Models\Channel;
use WindowsAzure\MediaServices\Models\ChannelEncoding;
use WindowsAzure\MediaServices\Models\ChannelEncodingPresets;
use WindowsAzure\MediaServices\Models\ChannelInput;
use WindowsAzure\MediaServices\Models\ChannelInputAccessControl;
use WindowsAzure\MediaServices\Models\ChannelPreview;
use WindowsAzure\MediaServices\Models\ChannelPreviewAccessControl;
use WindowsAzure\MediaServices\Models\ContentKey;
use WindowsAzure\MediaServices\Models\ContentKeyAuthorizationPolicy;
use WindowsAzure\MediaServices\Models\ContentKeyAuthorizationPolicyOption;
use WindowsAzure\MediaServices\Models\ContentKeyAuthorizationPolicyRestriction;
use WindowsAzure\MediaServices\Models\ContentKeyDeliveryType;
use WindowsAzure\MediaServices\Models\ContentKeyRestrictionType;
use WindowsAzure\MediaServices\Models\ContentKeyTypes;
use WindowsAzure\MediaServices\Models\CrossSiteAccessPolicies;
use WindowsAzure\MediaServices\Models\EncodingType;
use WindowsAzure\MediaServices\Models\IPAccessControl;
use WindowsAzure\MediaServices\Models\IPRange;
use WindowsAzure\MediaServices\Models\Locator;
use WindowsAzure\MediaServices\Models\Program;
use WindowsAzure\MediaServices\Models\ProtectionKeyTypes;
use WindowsAzure\MediaServices\Models\StreamingProtocol;
use WindowsAzure\MediaServices\Models\Task;
use WindowsAzure\MediaServices\Models\Job;
use WindowsAzure\MediaServices\Models\TaskOptions;
use WindowsAzure\MediaServices\Templates\SymmetricVerificationKey;
use WindowsAzure\MediaServices\Templates\TokenClaim;
use WindowsAzure\MediaServices\Templates\TokenRestrictionTemplate;
use WindowsAzure\MediaServices\Templates\TokenRestrictionTemplateSerializer;
use WindowsAzure\MediaServices\Templates\TokenType;


class AzureMediaService {
    /**
     * @var MediaServicesRestProxy
     */
    public $restProxy;
    
    public function __construct()
    {
        // Create proxy
        $this->restProxy = ServicesBuilder::getInstance()
            ->createMediaServicesService(
                new MediaServicesSettings('gakkoplus', 'HjIXZ/SOaSGQ5w50DVwZHC0yH6j3DCzzcroUIGerHG8='));
    }

    /**
     * Test upload video to Azure Media Stream
     */
    public function testUploadVideo(){
        //$this->restProxy->getAsset($assetId);


        $fileName = public_path().'/video/BigBuckBunny.mp4';
        $tokenRestriction = true;
        $tokenType = TokenType::JWT;

        // 0 - set up the MediaServicesService object to call into the Media Services REST API.
        $this->restProxy = ServicesBuilder::getInstance()
            ->createMediaServicesService(
                new MediaServicesSettings('gakkoplus',
                'HjIXZ/SOaSGQ5w50DVwZHC0yH6j3DCzzcroUIGerHG8='));
        // 1 - Upload the mezzanine
        $sourceAsset = $this->uploadFileAndCreateAsset($this->restProxy, $fileName);

        // 2 - encode the output asset
        $encodedAsset = $this->encodeToAdaptiveBitrateMP4Set($this->restProxy, $sourceAsset);


//        // 3 - Create Content Key
//        $contentKey = $this->createCommonTypeContentKey($this->restProxy, $encodedAsset);
//        dd($contentKey);



//        // 4 - Create the ContentKey Authorization Policy
//        $tokenTemplateString = null;
//        if ($tokenRestriction) {
//            $tokenTemplateString = addTokenRestrictedAuthorizationPolicy($this->restProxy, $contentKey, $tokenType);
//        } else {
//            addOpenAuthorizationPolicy($this->restProxy, $contentKey);
//        }
//        // 5 - Create the AssetDeliveryPolicy
//        createAssetDeliveryPolicy($this->restProxy, $encodedAsset, $contentKey);
        // 6 - Publish
        $streamUrl = $this->publishEncodedAsset($encodedAsset);
        dd($streamUrl);
    }

    public function testLiveStream(){
        // allow the script to run longer than 600 seconds (overrides userconfig.php)
        set_time_limit(0);

        // General Options
        $options = new \stdClass();
        $options->channelName = 'phpsdk-sample';
        $options->programName = 'program-sample';
        // Encoding Options
        $options->encodingType = EncodingType::None;
        $options->ingestProtocol = StreamingProtocol::RTMP;
        // Encoding Standard Options
        $options->archiveWindowLenght = new \DateInterval("PT1H");
        // AES Dynamic Encription Options
        $options->aesDynamicEncription = true;
        $options->tokenRestriction = true;
        $options->tokenType = TokenType::JWT;
        // Archive options
        $options->deleteArchiveAsset = true; // change this to false to keep the asset archive

        // 1 - Create and Start new Channel.
        $channel = $this->createAndStartChannel($options);

        // 2 - Create the Live Streaming Asset
        $assetResult = $this->createAsset($options);

        // 3 - Create and Start new Program, also apply AES encryption (if set)
        $program = $this->createAndStartProgram($assetResult->asset, $channel, $options);

        // 4 - Publish the Live Streaming Asset
        $assetResult->streamingUrl = $this->publishLiveAsset($assetResult->asset);

        // 5 - Display the ingest URL, the preview URL, the token (if applies) and the Streaming URL
        echo "Ingest URL: {$channel->getInput()->getEndpoints()[0]->getUrl()}".PHP_EOL;
        echo "Preview URL: {$channel->getPreview()->getEndpoints()[0]->getUrl()}".PHP_EOL;
        echo "Streaming URL: {$assetResult->streamingUrl}".PHP_EOL;
        if (isset($assetResult->generatedTestToken)) {
            echo "Token: Bearer {$assetResult->generatedTestToken}".PHP_EOL;
        }

        // Ingest URL
//        // 6 - Wait for user decide to shutdown.
//        echo PHP_EOL."Press ENTER to shutdown the live stream and cleanup resources.".PHP_EOL;
//        $handle = fopen ("php://stdin","r");
//        $line = fgets($handle);
        // 7 - Cleanup the entities.

        $this->cleanup($channel, $program, $options);
    }
    /**
     * Upload file and create asset on Azure media service
     * @param $filePath
     * @param $mediaFileName
     * @return Asset
     */
    public function uploadFileAndCreateAsset($filePath, $mediaFileName = null)
    {
        if(!$mediaFileName){
            $mediaFileName = basename($filePath);
        }
        // Create an empty "Asset" by specifying the name
        $asset = new Asset(Asset::OPTIONS_NONE);
        $asset->setName('Media File ' . $mediaFileName);
        $asset = $this->restProxy->createAsset($asset);
        $assetId = $asset->getId();

        // Create an Access Policy with Write permissions
        $accessPolicy = new AccessPolicy('UploadAccessPolicy');
        $accessPolicy->setDurationInMinutes(60.0);
        $accessPolicy->setPermissions(AccessPolicy::PERMISSIONS_WRITE);
        $accessPolicy = $this->restProxy->createAccessPolicy($accessPolicy);

        // Create a SAS Locator for the Asset
        $sasLocator = new Locator($asset,  $accessPolicy, Locator::TYPE_SAS);
        $sasLocator->setStartTime(new \DateTime('now -5 minutes'));
        $sasLocator = $this->restProxy->createLocator($sasLocator);

        // Get the mezzanine file content
        $fileContent = file_get_contents($filePath);

        // Use the 'uploadAssetFile' to perform a multi-part upload using the Block Blobs REST API storage operations
        $this->restProxy->uploadAssetFile($sasLocator, basename($filePath), $fileContent);

        // Notify Media Services that the file upload operation is done to generate the asset file metadata
        $this->restProxy->createFileInfos($asset);

        // Delete the SAS Locator (and Access Policy) for the Asset since we are done uploading files
        $this->restProxy->deleteLocator($sasLocator);
        $this->restProxy->deleteAccessPolicy($accessPolicy);

        return $asset;
    }

    /**
     * Get asset by id
     * @param $assetId
     * @return Asset
     */
    public function getAssetById($assetId){
        return $this->restProxy->getAsset($assetId);
    }

    /**
     * Encode an asset
     * @param Asset $asset
     * @return Asset
     */
    public function encodeToAdaptiveBitrateMP4Set( Asset $asset)
    {
        // 2.1 retrieve the latest 'Media Encoder Standard' processor version
        $mediaProcessor = $this->restProxy->getLatestMediaProcessor('Media Encoder Standard');

        // 2.2 Create the Job; this automatically schedules and runs it
        $outputAssetName = 'Encoded '.$asset->getName();
        $outputAssetCreationOption = Asset::OPTIONS_NONE;
        $taskBody = '<?xml version="1.0" encoding="utf-8"?><taskBody><inputAsset>JobInputAsset(0)</inputAsset><outputAsset assetCreationOptions="'.$outputAssetCreationOption.'" assetName="'.$outputAssetName.'">JobOutputAsset(0)</outputAsset></taskBody>';

        $task = new Task($taskBody, $mediaProcessor->getId(), TaskOptions::NONE);
        $task->setConfiguration('H264 Multiple Bitrate 720p');

        $job = new Job();
        $job->setName('Encoding Job');

        $job = $this->restProxy->createJob($job, array($asset), array($task));

        // 2.3 Check to see if the Job has completed
        $result = $this->restProxy->getJobStatus($job);

        $jobStatusMap = array('Queued', 'Scheduled', 'Processing', 'Finished', 'Error', 'Canceled', 'Canceling');

        while ($result != Job::STATE_FINISHED && $result != Job::STATE_ERROR && $result != Job::STATE_CANCELED) {
            sleep(5);
            $result = $this->restProxy->getJobStatus($job);
        }

        if ($result != Job::STATE_FINISHED) {
            exit(-1);
        }

        // 2.4 Get output asset
        $outputAssets = $this->restProxy->getJobOutputMediaAssets($job);
        $encodedAsset = $outputAssets[0];

        return $encodedAsset;
    }

    public function createCommonTypeContentKey($encodedAsset)
    {
        // 3.1 Generate a new key
        $aesKey = Utilities::generateCryptoKey(16);
        // 3.2 Get the protection key id for ContentKey
        $protectionKeyId = $this->restProxy->getProtectionKeyId(ContentKeyTypes::COMMON_ENCRYPTION);
        $protectionKey = $this->restProxy->getProtectionKey($protectionKeyId);
        $contentKey = new ContentKey();
        $contentKey->setContentKey($aesKey, $protectionKey);
        $contentKey->setProtectionKeyId($protectionKeyId);
        $contentKey->setProtectionKeyType(ProtectionKeyTypes::X509_CERTIFICATE_THUMBPRINT);
        $contentKey->setContentKeyType(ContentKeyTypes::COMMON_ENCRYPTION);
        // 3.3 Create the ContentKey
        $contentKey = $this->restProxy->createContentKey($contentKey);
        echo "Content Key id={$contentKey->getId()}".PHP_EOL;
        // 3.4 Associate the ContentKey with the Asset
        $this->restProxy->linkContentKeyToAsset($encodedAsset, $contentKey);
        return $contentKey;
    }


    /**
     * Publish a encoded assets
     * @param $encodedAsset
     * @return string
     */
    public function publishEncodedAsset($encodedAsset)
    {
        // 6.1 Get the .ISM AssetFile
        $files = $this->restProxy->getAssetAssetFileList($encodedAsset);
        $manifestFile = null;

        foreach ($files as $file) {
            $fileParts = pathinfo($file->getName());
            if ($fileParts['extension'] == 'ism') {
                $manifestFile = $file;
                break;
            }
        }

        if ($manifestFile == null) {
            echo "Unable to found the manifest file".PHP_EOL;
            return null;
        }

        // 6.2 Create a 30-day read-only AccessPolicy
        $access = new AccessPolicy('Streaming Access Policy');
        $access->setDurationInMinutes(60 * 24 * 30);
        $access->setPermissions(AccessPolicy::PERMISSIONS_READ);
        $access = $this->restProxy->createAccessPolicy($access);

        // 6.3 Create a Locator using the AccessPolicy and Asset
        $locator = new Locator($encodedAsset, $access, Locator::TYPE_ON_DEMAND_ORIGIN);
        $locator->setName('Streaming Locator');
        $locator = $this->restProxy->createLocator($locator);

        // Azure needs time to publish media
        sleep(30);

        // 6.4 Create a Smooth Streaming base URL
        $streamingUrl = $locator->getPath() . $manifestFile->getName().'/manifest';

        return $streamingUrl;
    }


    function runIndexingJob(Asset $asset, $taskConfiguration)
    {
        // Retrieve the latest 'Azure Media Indexer' processor version
        $mediaProcessor = $this->restProxy->getLatestMediaProcessor('Azure Media Indexer');

        echo "Using Media Processor: {$mediaProcessor->getName()} version {$mediaProcessor->getVersion()}".PHP_EOL;

        // Create the Job; this automatically schedules and runs it
        $outputAssetName = 'Indexer Results '. $asset->getName();
        $outputAssetCreationOption = Asset::OPTIONS_NONE;
        $taskBody = '<?xml version="1.0" encoding="utf-8"?><taskBody><inputAsset>JobInputAsset(0)</inputAsset><outputAsset assetCreationOptions="'.$outputAssetCreationOption.'" assetName="'.$outputAssetName.'">JobOutputAsset(0)</outputAsset></taskBody>';

        $task = new Task($taskBody, $mediaProcessor->getId(), TaskOptions::NONE);
        $task->setName('Indexing Task');
        $task->setConfiguration($taskConfiguration);

        $job = new Job();
        $job->setName('Indexing Job');

        $job = $this->restProxy->createJob($job, array($asset), array($task));

        echo "Created Job with Id: {$job->getId()}".PHP_EOL;

        // Check to see if the Job has completed
        $result = $this->restProxy->getJobStatus($job);

        $jobStatusMap = array('Queued', 'Scheduled', 'Processing', 'Finished', 'Error', 'Canceled', 'Canceling');

        while ($result != Job::STATE_FINISHED && $result != Job::STATE_ERROR && $result != Job::STATE_CANCELED) {
            echo "Job status: {$jobStatusMap[$result]}".PHP_EOL;
            sleep(5);
            $result = $this->restProxy->getJobStatus($job);
        }

        if ($result != Job::STATE_FINISHED) {
            echo "The job has finished with a wrong status: {$jobStatusMap[$result]}".PHP_EOL;
            exit(-1);
        }

        echo "Job Finished!".PHP_EOL;

        // Get output asset
        $outputAssets = $this->restProxy->getJobOutputMediaAssets($job);
        $outputAsset = $outputAssets[0];

        echo "Output asset: name={$outputAsset->getName()} id={$outputAsset->getId()}".PHP_EOL;

        return $outputAsset;
    }

    function downloadAssetFiles($asset, $destinationPath)
    {
        // Create destination directory if does not exist
        if (!file_exists($destinationPath)) {
            mkdir($destinationPath);
        }

        // Create an Access Policy with Read permissions
        $accessPolicy = new AccessPolicy('DownloadAccessPolicy');
        $accessPolicy->setDurationInMinutes(60.0);
        $accessPolicy->setPermissions(AccessPolicy::PERMISSIONS_READ);
        $accessPolicy = $this->restProxy->createAccessPolicy($accessPolicy);

        // Create a SAS Locator for the Asset
        $sasLocator = new Locator($asset, $accessPolicy, Locator::TYPE_SAS);
        $sasLocator->setStartTime(new \DateTime('now -5 minutes'));
        $sasLocator = $this->restProxy->createLocator($sasLocator);

        $files = $this->restProxy->getAssetAssetFileList($asset);

        // Download Asset Files
        foreach ($files as $file) {
            echo "Downloading {$file->getName()} output file...";
            $this->restProxy->downloadAssetFile($file, $sasLocator, $destinationPath);
            echo "Done!".PHP_EOL;
        }

        // Clean up Locator and Access Policy
        $this->restProxy->deleteLocator($sasLocator);
        $this->restProxy->deleteAccessPolicy($accessPolicy);
    }


    /**
     * @param MediaServicesRestProxy $this->restProxy
     * @param $options
     * @return Channel
     */
    public function createAndStartChannel($options)
    {
        // 1 - prepare the channel template
        $channelData = $this->createChannelData($options);

        // 2 - create the channel
        $channel = $this->restProxy->createChannel($channelData);

        // 3 - start the channel
        $this->restProxy->startChannel($channel);

        // 4 - get and return the started channel
        return $this->restProxy->getChannel($channel);
    }

    public function createAsset($options)
    {
        $result = new \stdClass();

        // 1 - prepare the program asset
        $result->asset = new Asset(Asset::OPTIONS_NONE);
        $result->asset->setName($options->programName);
        $result->asset = $this->restProxy->createAsset($result->asset);

        // 2 - create & apply asset delivey policy + AES encryption
        if ($options->aesDynamicEncription) {
            // Applying AES Dynamic Encryption... ";
            $result->generatedTestToken = $this->applyAesDynamicEncryption($result->asset, $options);
        } else {
            $this->applyNonDynamicEncription($result->asset);
        }

        return $result;
    }

    public function applyAesDynamicEncryption ($asset, $options)
    {
        $testToken = null;
        // 1 - Create Content Key
        $contentKey = $this->createEnvelopeTypeContentKey($asset);
        // 2 - Create the ContentKey Authorization Policy and Options
        if ($options->tokenRestriction) {
            // 2.1 - Apply Token restriction
            $template = $this->addTokenRestrictedAuthorizationPolicy($contentKey, $options->tokenType);

            // 2.2 - Generate Test Token
            $testToken = $this->generateTestToken($template, $contentKey);
        } else {
            // 2.1 - Apply Open restriction
            $this->addOpenAuthorizationPolicy($contentKey);
        }
        // 3 - Create the AssetDeliveryPolicy
        $this->createAssetDeliveryPolicy($asset, $contentKey);
        return $testToken;
    }


    public function applyNonDynamicEncription($asset)
    {
        $policy = new AssetDeliveryPolicy();
        $policy->setName("Clear Policy");
        $policy->setAssetDeliveryProtocol(AssetDeliveryProtocol::SMOOTH_STREAMING | AssetDeliveryProtocol::DASH | AssetDeliveryProtocol::HLS);
        $policy->setAssetDeliveryPolicyType(AssetDeliveryPolicyType::NO_DYNAMIC_ENCRYPTION);

        $policy = $this->restProxy->createAssetDeliveryPolicy($policy);
        $this->restProxy->linkDeliveryPolicyToAsset($asset, $policy);
    }

    /**
     * Create and start Live Program
     * @param Asset $asset
     * @param Channel $channel
     * @param $options
     * @return Program
     */
    function createAndStartProgram(Asset $asset, Channel $channel, $options) {
        // 1 - create a new program and link to the asset and channel
        $program = new Program();
        $program->setName($options->programName);
        $program->setAssetId($asset->getId());
        $program->setArchiveWindowLength($options->archiveWindowLenght);
        $program->setChannelId($channel->getId());
        // 1- Creating program...
        $program = $this->restProxy->createProgram($program);
        // 2 - start the program
        $this->restProxy->startProgram($program);

        return $this->restProxy->getProgram($program);
    }

    /**
     * Create Live channel data
     * @param $options
     * @return Channel
     */
    public function createChannelData($options)
    {
        $channel = new Channel();
        $channel->setName($options->channelName);
        // 1 - Channel Input
        $channelInput = new ChannelInput();
        $channelAccessControl = new ChannelInputAccessControl();
        $channelAccessControl->setIP($this->createOpenIPAccessControl());
        $channelInput->setAccessControl($channelAccessControl);
        $channelInput->setStreamingProtocol($options->ingestProtocol);
        $channel->setInput($channelInput);
        // 2 - Channel Preview
        $channelPreview = new ChannelPreview();
        $channelAccessControl = new ChannelPreviewAccessControl();
        $channelAccessControl->setIP($this->createOpenIPAccessControl());
        $channelPreview->setAccessControl($channelAccessControl);
        $channel->setPreview($channelPreview);
        // 3 - Channel Encoding
        if ($options->encodingType == EncodingType::Standard) {
            $channel->setEncodingType(EncodingType::Standard);
            $channelEncoding = new ChannelEncoding();
            $channelEncoding->setSystemPreset(ChannelEncodingPresets::Default720p);
            $channel->setEncoding($channelEncoding);
        }  else {
            $channel->setEncodingType(EncodingType::None);
        }
        // 4 - cors rules
        $channel->setCrossSiteAccessPolicies(new CrossSiteAccessPolicies());
        return $channel;
    }

    public function createOpenIPAccessControl()
    {
        $iPAccessControl = new IPAccessControl();
        $iPRange = new IPRange();
        $iPRange->setName("default");
        $iPRange->setAddress("0.0.0.0");
        $iPRange->setSubnetPrefixLength("0");
        $iPAccessControl->setAllow(array($iPRange));
        return $iPAccessControl;
    }

    /**
     * Publish Live asset
     * @param $asset
     * @return string
     */
    public function publishLiveAsset($asset)
    {
        // 1 Get the .ISM AssetFile
        $files = $this->restProxy->getAssetAssetFileList($asset);
        $manifestFile = null;

        foreach ($files as $file) {
            if ($this->endsWith(strtolower($file->getName()), '.ism')) {
                $manifestFile = $file;
            }
        }

        if ($manifestFile == null) {
            echo "Unable to found the manifest file".PHP_EOL;
            exit(-1);
        }

        // 2 Create a 30-day read-only AccessPolicy
        $access = new AccessPolicy('Live Access Policy');
        $access->setDurationInMinutes(60 * 24 * 30);
        $access->setPermissions(AccessPolicy::PERMISSIONS_READ);
        $access = $this->restProxy->createAccessPolicy($access);

        // 3 Create a Locator using the AccessPolicy and Asset
        $locator = new Locator($asset, $access, Locator::TYPE_ON_DEMAND_ORIGIN);
        $locator->setName('Live Locator');
        $locator = $this->restProxy->createLocator($locator);

        // 4 Return the Smooth Streaming base URL
        return $locator->getPath().$manifestFile->getName().'/manifest';
    }

    /**
     * Clearn up all channel and program (for Live )
     * @param $channel
     * @param Program $program
     * @param $options
     */
    function cleanup($channel, Program $program, $options)
    {
        // Stopping program.
        $this->restProxy->stopProgram($program);

        //Deleting program...
        $this->restProxy->deleteProgram($program);

        // Stopping channel
        $this->restProxy->stopChannel($channel);

        // Deleting channel
        $this->restProxy->deleteChannel($channel);

        // cleanup asset
        if ($options->deleteArchiveAsset) {
            $asset = $this->restProxy->getAsset($program->getAssetId());
            // clean locators
            $locators = $this->restProxy->getAssetLocatorList($asset);
            foreach ($locators as $loc) {
                $this->restProxy->deleteLocator($loc);
            }
            // clean delivery policies
            $policies = $this->restProxy->getAssetLinkedDeliveryPolicy($asset);
            foreach($policies as $policy) {
                $this->restProxy->removeDeliveryPolicyFromAsset($asset, $policy);
                $this->restProxy->deleteAssetDeliveryPolicy($policy);
            }
            // cleanup content keys
            $keys = $this->restProxy->getAssetContentKeys($asset);
            foreach ($keys as $key) {
                $this->restProxy->removeContentKeyFromAsset($asset, $key);
                $authPolicyId = $key->getAuthorizationPolicyId();
                $this->restProxy->deleteContentKeyAuthorizationPolicy($authPolicyId);
            }

            $this->restProxy->deleteAsset($asset);
        } else {
            echo "Archive asset was not removed.";
        }
    }

    //////////////////// Helper Method /////////////////////////////

    /**
     * @param MediaServicesRestProxy $this->restProxy
     * @param $encodedAsset
     * @return ContentKey
     */
    public function createEnvelopeTypeContentKey($encodedAsset)
    {
        // 1 Generate a new key
        $aesKey = Utilities::generateCryptoKey(16);
        // 2 Get the protection key id for ContentKey
        $protectionKeyId = $this->restProxy->getProtectionKeyId(ContentKeyTypes::ENVELOPE_ENCRYPTION);
        $protectionKey = $this->restProxy->getProtectionKey($protectionKeyId);
        $contentKey = new ContentKey();
        $contentKey->setContentKey($aesKey, $protectionKey);
        $contentKey->setProtectionKeyId($protectionKeyId);
        $contentKey->setProtectionKeyType(ProtectionKeyTypes::X509_CERTIFICATE_THUMBPRINT);
        $contentKey->setContentKeyType(ContentKeyTypes::ENVELOPE_ENCRYPTION);
        // 3 Create the ContentKey
        $contentKey = $this->restProxy->createContentKey($contentKey);
        // 4 Associate the ContentKey with the Asset
        $this->restProxy->linkContentKeyToAsset($encodedAsset, $contentKey);
        return $contentKey;
    }

    public function addOpenAuthorizationPolicy(ContentKey $contentKey)
    {
        // 1 Create ContentKeyAuthorizationPolicyRestriction (Open)
        $restriction = new ContentKeyAuthorizationPolicyRestriction();
        $restriction->setName('ContentKey Authorization Policy Restriction');
        $restriction->setKeyRestrictionType(ContentKeyRestrictionType::OPEN);
        // 2 Create ContentKeyAuthorizationPolicyOption (AES)
        $option = new ContentKeyAuthorizationPolicyOption();
        $option->setName('ContentKey Authorization Policy Option');
        $option->setKeyDeliveryType(ContentKeyDeliveryType::BASELINE_HTTP);
        $option->setRestrictions(array($restriction));
        $option = $this->restProxy->createContentKeyAuthorizationPolicyOption($option);
        // 3 Create ContentKeyAuthorizationPolicy
        $ckapolicy = new ContentKeyAuthorizationPolicy();
        $ckapolicy->setName('ContentKey Authorization Policy');
        $ckapolicy = $this->restProxy->createContentKeyAuthorizationPolicy($ckapolicy);
        // 4 Link the ContentKeyAuthorizationPolicyOption to the ContentKeyAuthorizationPolicy
        $this->restProxy->linkOptionToContentKeyAuthorizationPolicy($option, $ckapolicy);
        // 5 Associate the ContentKeyAuthorizationPolicy with the ContentKey
        $contentKey->setAuthorizationPolicyId($ckapolicy->getId());
        $this->restProxy->updateContentKey($contentKey);
    }

    public function addTokenRestrictedAuthorizationPolicy(ContentKey $contentKey, $tokenType) {
        // 1 Create ContentKeyAuthorizationPolicyRestriction (Token Restricted)
        $tokenRestriction = $this->generateTokenRequirements($tokenType);
        $restriction = new ContentKeyAuthorizationPolicyRestriction();
        $restriction->setName('ContentKey Authorization Policy Restriction');
        $restriction->setKeyRestrictionType(ContentKeyRestrictionType::TOKEN_RESTRICTED);
        $restriction->setRequirements($tokenRestriction);
        // 2 Create ContentKeyAuthorizationPolicyOption (AES)
        $option = new ContentKeyAuthorizationPolicyOption();
        $option->setName('ContentKey Authorization Policy Option');
        $option->setKeyDeliveryType(ContentKeyDeliveryType::BASELINE_HTTP);
        $option->setRestrictions(array($restriction));
        $option = $this->restProxy->createContentKeyAuthorizationPolicyOption($option);
        // 3 Create ContentKeyAuthorizationPolicy
        $ckapolicy = new ContentKeyAuthorizationPolicy();
        $ckapolicy->setName('ContentKey Authorization Policy');
        $ckapolicy = $this->restProxy->createContentKeyAuthorizationPolicy($ckapolicy);
        // 4 Link the ContentKeyAuthorizationPolicyOption to the ContentKeyAuthorizationPolicy
        $this->restProxy->linkOptionToContentKeyAuthorizationPolicy($option, $ckapolicy);
        // 5 Associate the ContentKeyAuthorizationPolicy with the ContentKey
        $contentKey->setAuthorizationPolicyId($ckapolicy->getId());
        $this->restProxy->updateContentKey($contentKey);
        return $tokenRestriction;
    }
    
    public function createAssetDeliveryPolicy($asset, $contentKey)
    {
        // 1 Get the acquisition URL
        $acquisitionUrl = $this->restProxy->getKeyDeliveryUrl($contentKey, ContentKeyDeliveryType::BASELINE_HTTP);
        // 2 Generate the AssetDeliveryPolicy Configuration Key
        $randomKey = Utilities::generateCryptoKey(16);
        $configuration = [AssetDeliveryPolicyConfigurationKey::ENVELOPE_KEY_ACQUISITION_URL => $acquisitionUrl,
            AssetDeliveryPolicyConfigurationKey::ENVELOPE_ENCRYPTION_IV_AS_BASE64 => base64_encode($randomKey)];
        $confJson = AssetDeliveryPolicyConfigurationKey::stringifyAssetDeliveryPolicyConfigurationKey($configuration);
        // 3 Create the AssetDeliveryPolicy
        $adpolicy = new AssetDeliveryPolicy();
        $adpolicy->setName('Asset Delivery Policy');
        $adpolicy->setAssetDeliveryProtocol(AssetDeliveryProtocol::SMOOTH_STREAMING | AssetDeliveryProtocol::DASH | AssetDeliveryProtocol::HLS);
        $adpolicy->setAssetDeliveryPolicyType(AssetDeliveryPolicyType::DYNAMIC_ENVELOPE_ENCRYPTION);
        $adpolicy->setAssetDeliveryConfiguration($confJson);
        $adpolicy = $this->restProxy->createAssetDeliveryPolicy($adpolicy);
        // 4 Link the AssetDeliveryPolicy to the Asset
        $this->restProxy->linkDeliveryPolicyToAsset($asset, $adpolicy->getId());
    }
    
    public function generateTokenRequirements($tokenType)
    {
        $template = new TokenRestrictionTemplate($tokenType);
        $template->setPrimaryVerificationKey(new SymmetricVerificationKey());
        $template->setAudience('urn:contoso');
        $template->setIssuer('https://sts.contoso.com');
        $claims = array();
        $claims[] = new TokenClaim(TokenClaim::CONTENT_KEY_ID_CLAIM_TYPE);
        $template->setRequiredClaims($claims);
        return TokenRestrictionTemplateSerializer::serialize($template);
    }

    public function generateTestToken($tokenTemplateString, ContentKey $contentKey)
    {
        $template = TokenRestrictionTemplateSerializer::deserialize($tokenTemplateString);
        $contentKeyUUID = substr($contentKey->getId(), strlen('nb:kid:UUID:'));
        $expiration = strtotime('+12 hour');
        $token = TokenRestrictionTemplateSerializer::generateTestToken($template, null, $contentKeyUUID, $expiration);
        return $token;
    }

    public function endsWith($haystack, $needle)
    {
        $length = strlen($needle);
        if ($length == 0) {
            return true;
        }
        return substr($haystack, -$length) === $needle;
    }

}

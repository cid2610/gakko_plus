<?php

namespace App\Http\Controllers;

use App\Course;
use App\Lesson;
use App\Lib\AzureMediaService;
use App\Live;
use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Sentinel;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

class TopController extends Controller
{
    /**
     * Top screen
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index(Request $request)
    {
        // 新着の授業動画
        $lessons = Lesson::select('lessons.*', 'courses.title as course_title')
            ->leftJoin('courses', 'lessons.course_id', '=', 'courses.id')
            ->orderBy('lessons.id','DESC')
            ->get()
        ;

        $premiumUsers = User::where(['service_plan' => 'premium'])
            //->orderBy('count','DESC')
            ->limit(10)
            ->get()
        ;

        return view('top.index',compact('lessons','premiumUsers','categories'));
    }

    /**
     * Profile screen
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function getProfile(Request $request, $id){
        $user = User::find($id);

        if(!$user){
            return redirect()->route('top.error');
        }

        return view('top.profile', compact('user'));
    }

    /**
     * Edit profile screen (display)
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function editProfile(Request $request){
        $profile = Sentinel::getUser();
        return view('top.editprofile',compact('profile'));
    }

    /**
     * Edit profile screen (update)
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function updateProfile(Request $request){
        // Validate the form data
        if(!empty($request->input('password'))) {
            $result = $this->validate($request, [
                'password' => 'required',
                'password_retry' => 'required|min:6',
            ]);
        }
        $profile = Sentinel::getUser();
        $hashedPassword = $profile->password;
        $checked = false;
        if (Hash::check($request->password, $hashedPassword)) {
            $checked = true;
            // todo check validate
            $profile->password = Hash::make($request->password_retry);
        }else{
            $request->offsetUnset('password');
        }
        Sentinel::update($profile, $request->all());

        if(!empty($request->input('password_retry')) && $checked) {
                $profile->password = Hash::make($request->password_retry);
//                $new_password = bcrypt($request->input('password_retry'));
//                $profile->password = $new_password;
                $profile->save();
            }

        return view('top.profile');
    }

    /**
     * Search screen
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function search(Request $request){
        // Sets the parameters from the get request to the variables.
        $search = $request->get('keyword'); //<-- we use global request to get the param of URI

        $lessons = Lesson::with('users')->where('title','like','%'.$search.'%')
            ->orderBy('title')
            ->paginate(10);

        $courses = Course::with('users')->where('title','like','%'.$search.'%')
            ->orderBy('title')
            ->paginate(10);

        $categories = self::CATEGORIES;
        return view('top.search',compact('lessons','courses','categories'));
    }

    /**
     * My Lives function
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function myLives(Request $request){
        $user = Sentinel::getUser();
        if (!$user || $user->service_plan != 'premium') {
            return redirect()->route('auth.login.form');
        }

        $lives = Live::where(['user_id' => $user->getUserId()])->orderBy('id', 'DESC')->paginate(16);

        return view('top.my_lives', compact('lives'))
            ->with(['i' => ($request->input('page', 1) - 1) * 16]);
    }

    public function error(Request $request){
        return view('top.error');
    }
}

<?php

namespace App\Http\Controllers;

use App\Course;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Lesson;
use App\Lib\AzureStorageBlob;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use MicrosoftAzure\Storage\Blob\Models\Block;
use WindowsAzure\Common\Internal\MediaServicesSettings;
use WindowsAzure\Common\ServiceException;
use WindowsAzure\Common\ServicesBuilder;
use WindowsAzure\MediaServices\Models\EncodingType;
use WindowsAzure\MediaServices\Models\StreamingProtocol;
use WindowsAzure\MediaServices\Templates\TokenType;
use Sentinel;

class CourseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $categoryNo = $request->get('category');
        if($categoryNo){
            //$whereCondition = "FIND_IN_SET($categoryNo, category)";
            // Change for SQL Server
            $whereCondition = "CONCAT(',', category, ',') LIKE '%,$categoryNo,%'";
        }else{
            $whereCondition = '1 = 1';
        }

        $courses = Course::whereRaw($whereCondition)->orderBy('id','DESC')->paginate(16);
        return view('courses.index',compact('courses'))
            ->with(['i' => ($request->input('page', 1) - 1) * 16,
                    'categories' => self::CATEGORIES,
                    'categoryNo' => $categoryNo
            ])
        ;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        return view('courses.create', [ 'categories' => self::CATEGORIES]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $user = Sentinel::getUser();
        if(!$user || $user->service_plan != 'premium'){
            return route('auth.login.form');
        }

        $this->validate($request, [
            'title' => 'required',
        ]);

        //Course::create($request->all());
        $category_checked = $request->get('category');
        $cats = '';
        if(is_array($category_checked))
        {
            $cats = implode(",",$category_checked);
        }
//        // TODO upload using AzureStorageBlob
//        $storage = new AzureStorageBlob();
//        $account = 'huunguyen';
//        $secret = 'A3VwOAbF2at2b2qfZLMeRGEC4/kmTHBgdkRoBehRtPM=';
//        $file_path =  $_FILES['image']['tmp_name'];
//        // $storage->upload('devcontain', $file_path, 'TestFile');
//        $tempalteFileName = asset('indexerTaskPresetTemplate.xml');
//        $destinationPath = asset('images');
//        $indexerTaskPresetTemplate = file_get_contents($tempalteFileName);
//        // Configuration parameters for Indexer task
//        $title = '';
//        $description = '';
//        $language = 'English';
//        $captionFormats = 'ttml;sami;webvtt';
//        $generateAIB = 'true';
//        $generateKeywords = 'true';
//        echo "Azure SDK for PHP - Media Analytics Sample (Indexer)".PHP_EOL;
//
//        // 0 - Set up the MediaServicesService object to call into the Media Services REST API.
//        $restProxy = ServicesBuilder::getInstance()->createMediaServicesService(new MediaServicesSettings($account, $secret));
//
//        // 1 - Upload the mezzanine
//        $sourceAsset = $storage->uploadFileAndCreateAsset($restProxy, $file_path);
//
//        // 2 - Create indexing task configuration based on parameters
//        $taskConfiguration = sprintf($indexerTaskPresetTemplate, $title, $description, $language, $captionFormats, $generateAIB, $generateKeywords);
//
//        // 3 - Run indexing job to generate output asset
//        $outputAsset = $storage->runIndexingJob($restProxy, $sourceAsset, $taskConfiguration);
//
//        // 4 - Download output asset files
//        $storage->downloadAssetFiles($restProxy, $outputAsset, $destinationPath);
//        // Done
//        echo 'Done!';
        $imageName = $request->get('image');
        $course = new Course(array(
            'title'     => $request->get('title'),
            'overview'  => $request->get('overview'),
            'category'  => $cats,
            'image'     => $imageName,
            'user_id'   => $user->getUserId()
        ));
        $course->save();

        return redirect()->route('courses.index')
            ->with('success','講座を作成しました');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $course = Course::find($id);

        $lessons = [];
        if($course){
            $lessons = Lesson::where(['course_id' => $course['id']])->get();
        }else{
            return redirect()->route('top.error');
        }

        $categoryNames = [];
        if($course->category){
            $categories = explode(',', $course->category);
            foreach ($categories as $category){
                $categoryNames[] = self::CATEGORIES[$category];
            }
        }

        return view('courses.show',compact('course', 'lessons', 'categoryNames'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        // Check user
        $user = Sentinel::getUser();
        if(!$user || $user->service_plan != 'premium'){
            return redirect()->route('auth.login.form');
        }

        // 講座情報を取得
        $course = Course::where([
            'user_id' => $user->getUserId(),
            'id' => $id,
        ])->first();

        // 授業一覧取得
        $lessons = [];
        if($course){
            $lessons = Lesson::find(['course_id' => $course['id']]);
        }else{
            return redirect()->route('top.error');
        }

        return view('courses.edit', [
            'course' => $course,
            'categories' => self::CATEGORIES,
            'lessons' => $lessons
        ]);
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required',
        ]);

        $course = Course::find($id);
        $category_checked = $request->get('category');
        $cats = '';
        if(is_array($category_checked))
        {
            $cats = implode(",",$category_checked);
        }

        if ($request->hasFile('image')) {
            $imageName = $request->file('image')->getClientOriginalName();
            $request->file('image')->move(
                public_path() . '/images/courses/', $imageName
            );
        }
        Course::find($id)->update(array(
            'title' => $request->get('title'),
            'overview'  => $request->get('overview'),
            'category'  => $cats,
            'image' => ($request->hasFile('image') != null) ? $request->file('image')->getClientOriginalName(): $course->image,
            'user_id' => \Sentinel::getUser()
        ));
        return redirect()->route('courses.index')
            ->with('success','講座を更新しました');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        Course::find($id)->delete();
        return redirect()->route('courses.index')
            ->with('success','Item deleted successfully');
    }


    /**
     * @param $con
     * @param $file_path
     * @param $filename
     * @return bool
     */
    public function upload($con, $file_path, $filename) {
        define("CONTAINERNAME", $con);
        define("FILENAME", $file_path);
        define("BLOCKBLOBNAME", basename($filename));
        try {
            if (null == CONNECTION_STRING || "" == CONNECTION_STRING)
            {
                exit();
            }

            $blobRestProxy = ServicesBuilder::getInstance()->createBlobService(CONNECTION_STRING);

            if (!file_exists(FILENAME))
            {
                exit();
            }

            $handle = fopen(FILENAME, "r");

            // Upload the blob using blocks.
            $counter = 1;
            $blockIds = array();

            while (!feof($handle))
            {
                $blockId = str_pad($counter, PADLENGTH, "0", STR_PAD_LEFT);

                $block = new Block();
                $block->setBlockId(base64_encode($blockId));
                $block->setType("Uncommitted");
                array_push($blockIds, $block);

                $data = fread($handle, BLOCKSIZE);

                // Upload the block.
                $blobRestProxy->createBlobBlock(CONTAINERNAME, BLOCKBLOBNAME, base64_encode($blockId), $data);
                $counter++;
            }

            // Done creating the blocks. Close the file and commit the blocks.
            fclose($handle);

            $blobRestProxy->commitBlobBlocks(CONTAINERNAME, BLOCKBLOBNAME, $blockIds);

        }
        catch(ServiceException $serviceException)
        {
            echo "ServiceException encountered.\n";
            $code = $serviceException->getCode();
            $error_message = $serviceException->getMessage();
            echo "$code: $error_message";
        }
        catch (\Exception $exception)
        {
            echo "Exception encountered.\n";
            $code = $exception->getCode();
            $error_message = $exception->getMessage();
            echo "$code: $error_message";
        }

        return true;
    }
}

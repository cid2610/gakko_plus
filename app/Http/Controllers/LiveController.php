<?php

namespace App\Http\Controllers;

use App\Lib\AzureMediaService;
use App\Live;
use Illuminate\Http\Request;
use Sentinel;
use WindowsAzure\MediaServices\Models\EncodingType;
use WindowsAzure\MediaServices\Models\StreamingProtocol;
use WindowsAzure\MediaServices\Templates\TokenType;

class LiveController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $now = new \DateTime();

        $lives = Live::where([
            ['end_time', '>=', $now],
            ['publish', '=', 1]
        ])->orderBy('id', 'DESC')->paginate(16);

        return view('lives.index', compact('lives'))
            ->with(['i' => ($request->input('page', 1) - 1) * 16]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user = Sentinel::getUser();
        if (!$user || $user->service_plan != 'premium') {
            return redirect()->route('auth.login.form');
        }

        return view('lives.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user = Sentinel::getUser();
        if (!$user || $user->service_plan != 'premium') {
            return redirect()->route('auth.login.form');
        }

        $this->validate($request, [
            'title' => 'required|string',
            'image' => 'required',
            'start_date' => 'required|string',
            'start_time' => 'required|string',
            'end_date' => 'required|string',
            'end_time' => 'required|string',
            'program_name' => 'required|string',
            'channel_name' => 'required|string|unique:lives',
        ]);

        $data = $request->all();
        $imageName = $request->file('image')->getClientOriginalName();

        $live = new Live([
            'user_id' => $user->getUserId(),
            'title' => 'title',
            'image' => $imageName,
            'start_time' => $data['start_date'] . ' ' . $data['start_time'] . ':00',
            'end_time' => $data['end_date'] . ' ' . $data['end_time'] . ':00',
            'program_name' => $data['program_name'],
            'channel_name' => $data['channel_name'],
        ]);
        $live->save();

        $request->file('image')->move(
            public_path() . '/images/lives', $imageName
        );

        return redirect()->route('lives.show', ['id' => $live->id]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $user = Sentinel::getUser();
        if (!$user || $user->service_plan != 'premium') {
            return redirect()->route('auth.login.form');
        }

        $live = Live::where(['user_id' => $user->getUserId(), 'id' => $id])->first();
        if (!$live) {
            return redirect()->route('top.error');
        }

        return view('lives.show', compact('live'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * Publish a Live Stream
     * @param $id
     * @return \Illuminate\Http\RedirectResponse|string
     */
    public function publish($id)
    {
        // Check user and live
        $user = Sentinel::getUser();
        if (!$user || $user->service_plan != 'premium') {
            return redirect()->route('auth.login.form');
        }

        $live = Live::where(['user_id' => $user->getUserId(), 'id' => $id])->first();
        if (!$live) {
            return redirect()->route('top.error');
        }

        // Create Media Service Live Changel and program
        $mediaService = new AzureMediaService();
        // General Options
        $options = new \stdClass();
        $options->channelName = $live->channel_name;
        $options->programName = $live->program_name;
        // Encoding Options
        $options->encodingType = EncodingType::None;
        $options->ingestProtocol = StreamingProtocol::RTMP;
        // Encoding Standard Options
        $options->archiveWindowLenght = new \DateInterval("PT1H");
        // AES Dynamic Encription Options
        $options->aesDynamicEncription = true;
        $options->tokenRestriction = true;
        $options->tokenType = TokenType::JWT;
        // Archive options
        $options->deleteArchiveAsset = true; // change this to false to keep the asset archive

        // 1 - Create and Start new Channel.
        $channel = $mediaService->createAndStartChannel($options);

        // 2 - Create the Live Streaming Asset
        $assetResult = $mediaService->createAsset($options);

        // 3 - Create and Start new Program, also apply AES encryption (if set)
        $program = $mediaService->createAndStartProgram($assetResult->asset, $channel, $options);

        // 4 - Publish the Live Streaming Asset
        $assetResult->streamingUrl = $mediaService->publishLiveAsset($assetResult->asset);

        // 5 - Display the ingest URL, the preview URL, the token (if applies) and the Streaming URL
//        echo "Ingest URL: {$channel->getInput()->getEndpoints()[0]->getUrl()}".PHP_EOL;
//        echo "Preview URL: {$channel->getPreview()->getEndpoints()[0]->getUrl()}".PHP_EOL;
//        echo "Streaming URL: {$assetResult->streamingUrl}".PHP_EOL;
//        if (isset($assetResult->generatedTestToken)) {
//            echo "Token: Bearer {$assetResult->generatedTestToken}".PHP_EOL;
//        }

        // Update Live
        $live['ingest_url'] = $channel->getInput()->getEndpoints()[0]->getUrl();
        $live['preview_url'] = $channel->getPreview()->getEndpoints()[0]->getUrl();
        $live['streaming_url'] = $assetResult->streamingUrl;
        $live->publish = 1;
        $live->channel_id = $channel->getId();
        $live->program_id = $program->getId();
        $live->save();

        return redirect()->route('lives.show', ['id'=> $live->id])->with(['success' => '生放送を公開しました']);
    }

    /**
     * Unpublish a Channel and Program
     * @param $id
     */
    public function unPublish($id)
    {
        // Check user and live
        $user = Sentinel::getUser();
        if (!$user || $user->service_plan != 'premium') {
            return route('auth.login.form');
        }

        $live = Live::where(['user_id' => $user->getUserId(), 'id' => $id])->first();
        if (!$live) {
            return redirect()->route('top.error');
        }
        // General Options
        $options = new \stdClass();
        $options->deleteArchiveAsset = true; // change this to false to keep the asset archive

        // Create Media Service Live Changel and program
        $mediaService = new AzureMediaService();
        $restProxy = $mediaService->restProxy;
        if($live->channel_id && $live->program_id){
            $channel = $restProxy->getChannel($live->channel_id);
            $program = $restProxy->getProgram($live->program_id);

            $mediaService->cleanup($channel, $program, $options);

            // Update Live
            $live->channel_id = null;
            $live->program_id = null;
            $live->ingest_url = null;
            $live->preview_url = null;
            $live->streaming_url = null;

        }

        $live->publish = 0;
        $live->save();

        return redirect()->route('lives.show', ['id' => $live->id])->with(['success' => '生放送を非公開公しました']);
    }

    /**
     * Display a Live Stream
     * @param $id
     */
    public function display($id){
        $live = Live::find($id);
        if (!$live) {
            return redirect()->route('top.error');
        }

        return view('lives.display', compact('live'));
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: leminhtoan
 * Date: 8/15/17
 * Time: 07:07
 */

namespace App\Http\Controllers;


use App\Lib\AzureMediaService;
use Illuminate\Http\Request;

class UploadController extends Controller
{
    /**
     * Upload image API
     * @param Request $request
     * @param $type
     * @return \Illuminate\Http\JsonResponse
     */
    public function image(Request $request, $type){
        $file = $request->file('file');

        if($file){
            $realPath = $file->getRealPath();
            $imageName = $file->hashName();

            // Move file
            $uploadSuccess = $file->move(
                public_path() . '/images/'. $type, $imageName
            );

            if ($uploadSuccess) {
                return response()->json([
                    'type' => $type,
                    'name' => $imageName
                ]);
            }
        }

        return response()->json('Error', 400);
    }

    /**
     * @param Request $request
     * @param $type
     * @return \Illuminate\Http\JsonResponse
     */
    public function video(Request $request, $type){
        $file = $request->file('file');

        if($file){
            $videoPath = $file->getRealPath();
            $originalName = $file->getClientOriginalName();

            // Upload to AzureMediaService
            $mediaService = new AzureMediaService();
            $asset = $mediaService->uploadFileAndCreateAsset($videoPath, $originalName);
            // Encode
            $encodeAsset = $mediaService->encodeToAdaptiveBitrateMP4Set($asset);

            if ($asset) {
                return response()->json([
                    'type' => $type,
                    'asset_id' => $encodeAsset->getId()
                ]);
            }
        }

        return response()->json('Error', 400);
    }
}
<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    const CATEGORIES = [
        1 => 'WEB',
        2 => 'プログラミング',
        3 => 'ビジネス',
        4 => 'ライフハック・自己啓発',
        5 => '語学',
        6 => '写真・映像',
        7 => '料理',
        8 => 'アート・デザイン',
        9 => 'ハンドメイド・DIY',
        10 => 'ビューティー・ヘルス',
        11 => 'その他' ,
    ];

}

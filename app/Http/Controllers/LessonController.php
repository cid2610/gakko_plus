<?php

namespace App\Http\Controllers;

use App\Course;
use App\Lesson;
use App\Lib\AzureMediaService;
use Illuminate\Http\Request;
use Sentinel;

class LessonController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(Request $request)
    {
        $user = Sentinel::getUser();
        if(!$user){
            return redirect()->route('auth.login.form');
        }

        $lessons = Lesson::select('lessons.*', 'courses.title as course_title')
            ->leftJoin('courses', 'lessons.course_id', '=', 'courses.id')
            ->where([ 'lessons.user_id' => $user->getUserId()])
            ->orderBy('lessons.id','DESC')
            ->paginate(16);

        return view('lessons.index',compact('lessons'))
            ->with('i', ($request->input('page', 1) - 1) * 16);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        $user = Sentinel::getUser();
        if(!$user || $user->service_plan != 'premium'){
            return redirect()->route('auth.login.form');
        }

        $courses = Course::where( ['user_id' => $user->getUserId()])->pluck('title', 'id');

        $status = array( 0 => 'active', 1 => 'inactive');
        return view('lessons.create',compact('status', 'courses'));
    }
    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store(Request $request)
    {
        $user = Sentinel::getUser();
        if(!$user || $user->service_plan != 'premium'){
            return redirect()->route('auth.login.form');
        }

        $this->validate($request, [
            'asset_id' => 'required|string'
        ]);

        $courseId = $request->get('course');
        $title = $request->get('title');
        $start = $request->get('start');
        $end = $request->get('end');
        $thumbnail = $request->get('thumbnail');
        $assetId = $request->get('asset_id');

        // Encode and Publish and asset
        $mediaService = new AzureMediaService();
        $encodeAsset = $mediaService->getAssetById($assetId);
        // Publish
        $streamingUrl = $mediaService->publishEncodedAsset($encodeAsset);

        // Create course
        if($courseId == 'new'){
            $course = new Course([
                'title' => $title,
            ]);
            $course->save();
            $courseId = $course->id;
        }

        $fileName = null;
        if($thumbnail){
            list($type, $imageData) = explode(';', $thumbnail);
            list(,$extension) = explode('/',$type);
            list(,$imageData)      = explode(',', $imageData);
            $fileName = $title . uniqid().'.'.$extension;
            $imageData = base64_decode($imageData);
            file_put_contents(public_path() . "/images/lessons/$fileName", $imageData);
        }

        $chapter_name = '';
        if(is_array($start))
        {
            $chapter_name = implode("||",$start);
        }
        $chapter_time = '';
        if(is_array($start))
        {
            $chapter_time = implode("||",$end);
        }
        $lesson = new Lesson(array(
            'user_id' => $user->getUserId(),
            'title' => $request->get('title'),
            'chapter_name'  => $chapter_name,
            'chapter_time'  => $chapter_time,
            'course_id'  => $courseId,
            'thumbnail' => $fileName,
            'asset_id' => $assetId,
            'streaming_url' => $streamingUrl,
            'count' => 0
        ));

        $lesson->save();

        return redirect()->route('lessons.index')
            ->with('success','授業を作成しました');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        $lesson = Lesson::find($id);

        $chapterName = [];
        $chapterTime = [];
        if($lesson->chapter_name){
            $chapterName = explode('||', $lesson->chapter_name);
            $chapterTime = explode('||', $lesson->chapter_time);
        }

        return view('lessons.show',compact('lesson', 'chapterName', 'chapterTime'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function publish($id)
    {
        $user = Sentinel::getUser();
        if(!$user || $user->service_plan != 'premium'){
            return redirect()->route('auth.login.form');
        }

        $lesson = Lesson::where(['id' =>$id, 'user_id' => $user->getUserId()])->first();

        if(!$lesson){
            return redirect()->route('top.error');
        }

        return view('lessons.show',compact('lesson'));
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\View\View
     */
    public function postPublish($id)
    {
        $user = Sentinel::getUser();
        if(!$user || $user->service_plan != 'premium'){
            return redirect()->route('auth.login.form');
        }

        $lesson = Lesson::where(['id' =>$id, 'user_id' => $user->getUserId()])->first();

        if(!$lesson){
            return redirect()->route('top.error');
        }

        $assetId = $lesson->asset_id;
        if($assetId){
            $mediaService = new AzureMediaService();
            $asset = $mediaService->getAssetById($assetId);

            // Encode
            $encodeAsset = $mediaService->encodeToAdaptiveBitrateMP4Set($asset);

            // Publish
            $mediaService->publishEncodedAsset($encodeAsset);
        }

        return view('lessons.show',compact('lesson', 'chapterName', 'chapterTime'));
    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        $lesson = Lesson::find($id);
        $chapter_name = explode("||", $lesson->chapter_name);
        $chapter_time = explode("||", $lesson->chapter_time);
        return view('lessons.edit',compact('lesson','chapter_name','chapter_time'));
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $request, $id)
    {
        $this->validate($request, [
            'title' => 'required',
        ]);

        Lesson::find($id)->update($request->all());
        return redirect()->route('lessons.index')
            ->with('success','Item updated successfully');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        Lesson::find($id)->delete();
        return redirect()->route('lessons.index')
            ->with('success','Item deleted successfully');
    }

    public function uploadVideo(){


    }
}

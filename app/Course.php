<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    protected $fillable=[
        'title',
        'overview',
        'category',
        'image',
        'user_id'
    ];

    public function rules()
    {
        return [
            'title'        => 'required',
            'image'       => 'required|mimes:png'
        ];
    }

    protected $categorys = [
        'm' => 'Male',
        'f' => 'Female'
    ];

    public function getCategorys()
    {
        return $this->categorys;
    }

    public function users()
    {
        return $this->hasMany('App\User', 'id', 'user_id');
    }
}

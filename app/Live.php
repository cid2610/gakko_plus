<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Live extends Model
{
    //
    protected $fillable=[
        'user_id',
        'course_id',
        'title',
        'image',
        'start_time',
        'end_time',
        'channel_name',
        'chanel_id',
        'program_name',
        'program_id',
        'ingest_url',
        'preview_url',
        'streaming_url',
        'publish',
    ];
}

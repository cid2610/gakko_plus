<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Lesson extends Model
{
    protected $fillable=[
        'user_id',
        'course_id',
        'title',
        'status',
        'chapter_name',
        'chapter_time',
        'count',
        'thumbnail',
        'asset_id',
        'streaming_url',
    ];

    public function users()
    {
        return $this->hasMany('App\User', 'id', 'user_id');
    }
}

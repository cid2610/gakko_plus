<!DOCTYPE html>
<html>
    @include('layouts.header')

    <body class="widescreen fixed-left-void">
        <!-- Begin page -->
        <div id="wrapper" class="enlarged forced">
            <!-- Top Bar Start -->

            @include('layouts.topmenu')

            @include('layouts.leftmenu')

            <!-- Top Bar End -->
            <!-- ============================================================== -->
            <!-- Start right Content here -->
            <!-- ============================================================== -->
            <div class="content-page">
                    <!-- Start content -->
                    <div class="content">
                        <!-- content -->
                        <div class="wraper container">
                            @include('Centaur::notifications')
                            @yield('content')
                        </div>
                    </div>
                 <footer class="footer text-right">Copyright © 2017 ガッコウ＋株式会社. All Rights Reserved.</footer>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End right Content here -->
        <!-- ============================================================== -->
        <!--Facebookページ-->
        <div id="fb-root"></div>
        @include('layouts.footer')
    </body>
</html>
@extends('layout')
@section('content')
    <div class="row">
        <div class="col-sm-12">
            {{--<div class="btn-group pull-right m-t-15">--}}
                {{--<button type="button" class="btn btn-default dropdown-toggle waves-effect waves-light" data-toggle="dropdown" aria-expanded="false">講座作成 --}}
                    {{--<span class="m-l-5"><i class="fa fa-cog"></i></span>--}}
                {{--</button>--}}
                {{--<ul class="dropdown-menu drop-menu-right" role="menu">--}}
                    {{--<li><a href="#">Action</a></li>--}}
                    {{--<li><a href="#">Another action</a></li>--}}
                    {{--<li><a href="#">Something else here</a></li>--}}
                    {{--<li class="divider"></li>--}}
                    {{--<li><a href="#">Separated link</a></li>--}}
                {{--</ul>--}}
            {{--</div>--}}

            <h4 class="page-title">
                講座一覧
                @if(Sentinel::check())
                    @php($user = Sentinel::getUser())
                    @if($user['service_plan'] == 'premium')
                        <a class="btn btn-primary pull-right" href="{{ route('courses.create') }}"> 講座作成</a>
                    @endif
                @endif
            </h4>
            <ol class="breadcrumb">
                <li><a href="{{ route('top') }}">ホーム</a></li>
                <li class="active">講座一覧</li>
            </ol>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12 col-md-12 col-sm-12 ">
            <div class="portfolioFilter">
                <a href="{{route('courses.index')}}" data-filter="*" class="{{ $categoryNo == '' ? 'current' : '' }}">All</a>
                @foreach($categories as $no=>$category)
                    <a href="{{ route('courses.index', ['category' => $no]) }}" class="{{ $categoryNo == $no ? 'current' : '' }}">{{ $category }}</a>
                @endforeach
            </div>
        </div>
    </div>

    <div class="row port">
        <div class="portfolioContainer" style="position: relative; height: 1176px;">
            @foreach ($courses as $course)
            <div class="col-sm-6 col-lg-3 col-md-4 webdesign illustrator">
                <div class="gal-detail thumb">
                    <a href="{{ route('courses.show', ['id' => $course->id]) }}" class="image-popup" title="Screenshot-1">
                        <img src="{{ $course->image ? asset('images/courses/'. $course->image) : asset('images/courses/no-image.jpg') }}" class="thumb-img" alt="work-thumbnail">
                    </a>
                    <h4>{{ $course->title }}</h4>
                </div>
            </div>
            @endforeach
        </div>
        <div class="col-xs-12" >
            <div class="pull-right">
                {!! $courses->render() !!}
            </div>
        </div>
    </div>

@stop


@extends('layout')
@section('content')
    <div class="card-box" style="overflow: auto">
        <div class="col-xs-12" style="margin-bottom: 10px">
            <h3 class="panel-title">
                {{ $course->title }}
            </h3>
            <small>カテゴリ: {{ implode(', ', $categoryNames) }}</small>

            @php ($user = Sentinel::getUser())
            @if ($user['service_plan'] == 'premium')
            <div class="btn-group pull-right">
                <a href="{{route('courses.edit', ['id' => $course['id'] ])}}" class="btn btn-sm btn-primary waves-effect waves-light">編集<span class="m-l-5"><i class="fa fa-cog"></i></span>
                </a>
            </div>
            @endif
        </div>

        <div class="col-xs-8">
            <img src="{{ $course['image'] ? asset('images/courses/' . $course['image']) :  asset('images/courses/no-image.jpg')}}" width="400px" height="200px">
        </div>

        <div class="col-lg-4">
            <div class="panel panel-border panel-custom">
                <div class="panel-heading">
                    <h3 class="panel-title">次回の生放送</h3>
                </div>
                <div class="panel-body">
                    <p>
                        8月２２日
                    </p>
                    <p>
                        10時 ~ 15時
                    </p>
                </div>
            </div>
        </div>

        <div class="col-xs-12">
            <p>
                {!! nl2br($course->overview) !!}
            </p>
        </div>
    </div>

    <div class="card-box">
        <h3 class="panel-title">授業一覧</h3>
        <div class="col-xs-12 ">
            <div class="row port">
                <div class="portfolioContainer" style="position: relative;">
                    @foreach ($lessons as $lesson)
                        <div class="col-sm-6 col-lg-3 col-md-4 webdesign illustrator">
                            <div class="gal-detail thumb">
                                <a href="{{ route('lessons.show', ['id' => $lesson->id]) }}" class="image-popup" title="Screenshot-1">
                                    <img src="{{ $lesson->thumbnail ? asset('images/lessons/'. $lesson->thumbnail) : asset('images/lesson/no-image.jpg') }}" class="thumb-img" alt="work-thumbnail">
                                </a>
                                <h4>{{ $lesson->title }}</h4>
                            </div>
                        </div>
                    @endforeach
                    <p>
                    </p>
                </div>
            </div>
        </div>
    </div>
@stop


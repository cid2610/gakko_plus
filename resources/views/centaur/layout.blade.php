<!DOCTYPE html>
<html>

@include('layouts.header')
@section('title', 'ログイン')

<body class="widescreen fixed-left-void">
    <!-- Begin page -->
    <div id="wrapper" class="enlarged forced">
        <!-- Top Bar Start -->
        @include('layouts.topmenu')

        <div style="margin-top: 20px"></div>

        @yield('content')

        <script>
            var resizefunc = [];
        </script>
    </div>

    @include('layouts.footer')
</body>
</html>
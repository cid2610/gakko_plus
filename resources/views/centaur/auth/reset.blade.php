@extends('Centaur::layout')

@section('title', 'Resend Activation Instructions')

@section('content')
<div class="account-pages"></div>
<div class="clearfix"></div>

<div class="wrapper-page">
    <div class=" card-box">
        <div class="panel-heading">
            <h3 class="text-center"> パスワード再設定 </h3>
        </div>

        <div class="panel-body">
            <div class="alert alert-info alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">
                    ×
                </button>
                ご指定のメールアドレスにパスワード再設定のメールをお送りします！
            </div>
            <div class="form-group m-b-0">
                <form accept-charset="UTF-8" role="form" method="post" action="{{ route('auth.password.request.attempt') }}">
                    <fieldset>
                        <div class="form-group {{ ($errors->has('email')) ? 'has-error' : '' }}">
                            <input class="form-control" placeholder="E-mail" name="email" type="text" value="{{ old('email') }}">
                            {!! ($errors->has('email') ? $errors->first('email', '<p class="text-danger">:message</p>') : '') !!}
                        </div>
                        <input name="_token" value="{{ csrf_token() }}" type="hidden">
                        <input class="btn btn-lg btn-primary btn-block" type="submit" value="送信">
                    </fieldset>
                </form>
            </div>
        </div>
    </div>
</div>

@stop
@extends('Centaur::layout')

@section('title', 'Resend Activation Instructions')

@section('content')
<div class="account-pages"></div>
<div class="clearfix"></div>

<div class="wrapper-page">
    <div class=" card-box">
        <div class="panel-heading">
            <h3 class="text-center"> ご登録ありがとうございます ! </h3>
        </div>

        <div class="panel-body">
            @include('Centaur::notifications')
            <div class="text-center">
                <a class="btn btn-success" href="{{ route('top')}}">ホームへ戻る</a>
            </div>
        </div>
    </div>
</div>

@stop
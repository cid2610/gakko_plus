@extends('Centaur::layout')

@section('title', '会員登録')

@section('script')
    <script src="{{ asset('/js/pages/register.js') }}"></script>
@stop

@section('content')
<div class="account-pages"></div>
<div class="clearfix"></div>

<div class="wrapper-page signup-signin-page" style="width:80%;">
    <div class="card-box">
        <div class="text-center">
            <img src="{{ asset('images/logo_dark.png') }}" width="200">
        </div>
        <br>
        <div class="panel-heading">
            <h3 class="text-center">会員登録</h3>
        </div>

        <div class="panel-body">
            <div class="row">
                <div class="col-lg-12">
                    <div class="p-20">
                        <!-- <h4><b>Sign Up</b></h4> -->
                        <div class="p-20">
                            <table class="table table-bordered m-0">
                                <thead>
                                <tr>
                                    <th>アップグレード</th>
                                    <th>プレミアム</th>
                                </tr>
                                </thead>
                                <tbody>
                                <tr>
                                    <td>授業動画等が見放題</td>
                                    <td>アップグレード会員の機能<br>＋授業動画の投稿、生放送、コンテンツ販売等が可能</td>
                                </tr>
                                <tr>
                                    <td>月額:540円<br>※学生の場合は月額:390円</td>
                                    <td>月額:一定期間まで無料<br>※テスト(β)期間が終了して30日後に課金が開始<br>※講師としての審査がある</td>
                                </tr>
                                </tbody>
                            </table>
                        </div>

                        <br><br><br>

                        <div class="col-md-12">
                            <ul class="nav nav-tabs tabs tabs-top" style="width: 100%;">
                                <li class="tab active" style="width: 50%;">
                                    <a href="#home-2" data-toggle="tab" aria-expanded="true" class="active">
                                        <span class="visible-xs"><i class="fa fa-home"></i></span>
                                        <span class="hidden-xs">アップグレード</span>
                                    </a>
                                </li>
                                <li class="tab" style="width: 50%;">
                                    <a href="#profile-2" data-toggle="tab" aria-expanded="false" class="">
                                        <span class="visible-xs"><i class="fa fa-user"></i></span>
                                        <span class="hidden-xs">プレミアム</span>
                                    </a>
                                </li>
                                <div class="indicator" style="right: 324px; left: 0px;"></div></ul>
                            <div class="tab-content">
                                <!-- アップグレード フォーム -->
                                <div class="tab-pane active" id="home-2" style="display: block;">
                                    <form class="form-horizontal m-t-20" action="{{ route('auth.register.attempt') }}" method="post" enctype="multipart/form-data">
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">ユーザー名<div>※</div></label>
                                            <div class="col-md-7">
                                                <input type="text" name="username" id="username" class="form-control" placeholder="3～10文字" value="{{ old('username') }}">
                                                {!! ($errors->has('username') ? $errors->first('username', '<p class="text-danger">:message</p>') : '') !!}
                                            </div>
                                        </div>

                                        <hr>

                                        <div class="form-group">
                                            <label class="col-md-3 control-label">メールアドレス<div>※</div></label>
                                            <div class="col-md-7">
                                                <input type="email" name="email" id="email" class="form-control" parsley-type="email" value="{{old('email')}}">
                                                {!! ($errors->has('email') ? $errors->first('email', '<p class="text-danger">:message</p>') : '') !!}
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-3 control-label">パスワード<div>※</div></label>
                                            <div class="col-md-7 pass_input_box">
                                                <div class="col-md-6">
                                                    <input type="password" name="password" id="password" class="form-control" placeholder="8～20文字" value="{{old('password')}}">
                                                    {!! ($errors->has('password') ? $errors->first('password', '<p class="text-danger">:message</p>') : '') !!}
                                                </div>
                                                <div class="col-md-6">
                                                    <input type="password" name="password_confirmation" id="password_confirmation" class="form-control" data-parsley-equalto="#pass2" placeholder="確認用" value="{{old('password_confirmation')}}">
                                                    {!! ($errors->has('password_confirmation') ? $errors->first('password_confirmation', '<p class="text-danger">:message</p>') : '') !!}
                                                </div>
                                            </div>
                                        </div>

                                        <hr>

                                        <div class="form-group">
                                            <label for="field-7" class="col-md-3 control-label">性別<div id="re0">※</div></label>
                                            <div class="col-md-7">
                                                <div class="radio radio-info radio-inline">
                                                    <input type="radio" name="sex" id="inlineRadio1" value="man" {{ old('sex') == 'man' ? 'checked' : '' }}>
                                                    <label for="inlineRadio1"> 男性 </label>
                                                </div>

                                                <div class="radio radio-info radio-inline">
                                                    <input type="radio" name="sex" id="inlineRadio2" value="woman" {{ old('sex') == 'man' ? 'checked' : '' }}>
                                                    <label for="inlineRadio2"> 女性 </label>
                                                </div>
                                                {!! ($errors->has('sex') ? $errors->first('sex', '<p class="text-danger">:message</p>') : '') !!}
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-3 control-label">氏名<div id="re0">※</div></label>
                                            <div class="col-md-7 name_input_box">
                                                <div class="col-md-6">
                                                    <input id="first_name" type="text" name="first_name" class="form-control" data-parsley-length="[1,10]" placeholder="姓" value="{{ old('first_name') }}">
                                                    {!! ($errors->has('first_name') ? $errors->first('first_name', '<p class="text-danger">:message</p>') : '') !!}
                                                </div>
                                                <div class="col-md-6">
                                                    <input id="last_name" type="text" name="last_name" class="form-control" data-parsley-length="[1,10]" placeholder="名" value="{{ old('last_name') }}">
                                                    {!! ($errors->has('last_name') ? $errors->first('last_name', '<p class="text-danger">:message</p>') : '') !!}
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group birthday">
                                            <label class="col-md-3 control-label">生年月日</label>
                                            <div class="col-md-7">
                                                <div class="col-md-4">
                                                    <select  class="selectpicker year" data-live-search="false" data-style="btn-white">
                                                        <option>年</option>
                                                        <?php
                                                        for ($i = 1900; $i <= 2017; $i++) {
                                                            echo '<option>' . $i . '</option>';
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                                <div class="col-md-4">
                                                    <select class="selectpicker month" data-live-search="false" data-style="btn-white">
                                                        <option>月</option>
                                                        <?php for ($i = 1; $i <= 12; $i++) {
                                                            echo '<option>' . $i . '</option>';
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                                <div class="col-md-4">
                                                    <select  class="selectpicker day" data-live-search="false" data-style="btn-white">
                                                        <option>日</option>
                                                        <?php for ($i = 1; $i <= 31; $i++) {
                                                            echo '<option>' . $i . '</option>';
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                                <input type="hidden" name="birthday" value="{{ old('birthday') }}">
                                                {!! ($errors->has('birthday') ? $errors->first('birthday', '<p class="text-danger">:message</p>') : '') !!}
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-3 control-label">住所</label>
                                            <div class="col-md-7">
                                                <input type="text" name="location" id="location" class="form-control" placeholder="〇〇県....XX-XX 〇〇〇タワー8F 802号室" value="{{ old('location') }}">
                                                {!! ($errors->has('location') ? $errors->first('location', '<p class="text-danger">:message</p>') : '') !!}
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-3 control-label">電話番号</label>
                                            <div class="col-md-7">
                                                <input type="text" name="phone_number" id="phone_number" class="form-control" placeholder="000-0000-0000" value="{{ old('phone_number') }}">
                                                {!! ($errors->has('phone_number') ? $errors->first('phone_number', '<p class="text-danger">:message</p>') : '') !!}
                                            </div>
                                        </div>

                                        <hr>

                                        <div class="form-group">
                                            <label class="col-md-3 control-label">クレジットカード決済<div>※</div></label>
                                            <div class="col-md-7">
                                                <p class="text-muted font-13 m-b-30">
                                                    <!--
                                                    アップグレードの場合は初月は一切の無料で課金されません。
                                                    <br>一月後に自動的に月額決済が行われます。途中解約を行えば決済は行われません。

                                                    <br><br>-->※カード番号は「-」を抜いた半角数字で入力して下さい。
                                                </p>
                                            </div>

                                            <label class="col-md-3 control-label">カード番号<div>※</div></label>
                                            <div class="col-md-7">
                                                <input type="text" name="card_no" id="card_no" class="form-control" placeholder="0000111100001111">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-3 control-label">有効期限<div>※</div></label>
                                            <div class="col-md-7">
                                                <div class="col-md-6">
                                                    <select name="card_month" id="card_month" class="selectpicker" data-live-search="false" data-style="btn-white">
                                                        <option>月</option>
                                                        <?php for ($i = 1; $i <= 12; $i++) {
                                                            echo '<option value="'. sprintf('%02d', $i) .'">' . $i . '</option>';
                                                        }
                                                        ?>
                                                    </select>

                                                </div>

                                                <div class="col-md-6">
                                                    <select name="card_year" id="card_year" class="selectpicker" data-live-search="false" data-style="btn-white">
                                                        <option>年</option>
                                                        <?php for ($i = 2017; $i <= 2031; $i++) {
                                                            echo '<option>' . $i . '</option>';
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-3 control-label">クレジットカード名義人<div>※</div></label>
                                            <div class="col-md-7">
                                                <input type="text" name="card_name" id="card_name" class="form-control" placeholder="TARO YAMADA">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-3 control-label">セキュリティコード<div>※</div></label>
                                            <div class="col-md-7">
                                                <input type="text" name="card_code" id="card_code" class="form-control" placeholder="000">
                                            </div>
                                        </div>

                                        <hr>

                                        <div class="form-group">
                                            <label class="col-md-3 control-label">オプション</label>
                                            <div class="col-md-7">
                                                <div class="checkbox checkbox-pink">
                                                    <input type="checkbox" name="mailoption" id="mailoption" data-parsley-multiple="group1" value="on" {{ (old('mailoption')== '' || old('mailoption') == 'on') ? 'checked' : '' }}>
                                                    {!! ($errors->has('mailoption') ? $errors->first('mailoption', '<p class="text-danger">:message</p>') : '') !!}
                                                    <label for="checkbox4"> ガッコウ＋からのメールを受け取る<br>新着講義の情報や、キャンペーン情報をお届けします</label>
                                                </div>
                                            </div>
                                        </div>

                                        <br><br>

                                        <div class="form-group m-b-0">
                                            <label class="col-sm-4 control-label"></label>
                                            <div class="col-sm-5">
                                                <button type="submit" id="entry" class="btn btn-primary">
                                                    会員登録へ
                                                </button>
                                            </div>
                                        </div>
                                        <input type="hidden" name="service_plan"  value="upgrade">
                                        <input name="_token" value="{{ csrf_token() }}" type="hidden">
                                    </form>
                                </div>

                                <!-- プレミアム フォーム -->
                                <div class="tab-pane" id="profile-2" style="display: none;">
                                    <form class="form-horizontal m-t-20" action="{{ route('auth.register.attempt') }}" method="post" enctype="multipart/form-data">
                                        <div class="form-group">
                                            <label class="col-md-3 control-label">ユーザー名<div>※</div></label>
                                            <div class="col-md-7">
                                                <input type="text" name="username" id="username" class="form-control" placeholder="3～10文字" value="{{ old('username') }}">
                                                {!! ($errors->has('username') ? $errors->first('username', '<p class="text-danger">:message</p>') : '') !!}
                                            </div>
                                        </div>

                                        <hr>

                                        <div class="form-group">
                                            <label class="col-md-3 control-label">メールアドレス<div>※</div></label>
                                            <div class="col-md-7">
                                                <input type="email" name="email" id="email" class="form-control" parsley-type="email" value="{{old('email')}}">
                                                {!! ($errors->has('email') ? $errors->first('email', '<p class="text-danger">:message</p>') : '') !!}
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-3 control-label">パスワード<div>※</div></label>
                                            <div class="col-md-7 pass_input_box">
                                                <div class="col-md-6">
                                                    <input type="password" name="password" id="password" class="form-control" placeholder="8～20文字" value="{{old('password')}}">
                                                    {!! ($errors->has('password') ? $errors->first('password', '<p class="text-danger">:message</p>') : '') !!}
                                                </div>
                                                <div class="col-md-6">
                                                    <input type="password" name="password_confirmation" id="password_confirmation" class="form-control" data-parsley-equalto="#pass2" placeholder="確認用" value="{{old('password_confirmation')}}">
                                                    {!! ($errors->has('password_confirmation') ? $errors->first('password_confirmation', '<p class="text-danger">:message</p>') : '') !!}
                                                </div>
                                            </div>
                                        </div>

                                        <hr>

                                        <div class="form-group">
                                            <label for="field-7" class="col-md-3 control-label">性別<div id="re0">※</div></label>
                                            <div class="col-md-7">
                                                <div class="radio radio-info radio-inline">
                                                    <input type="radio" name="sex" id="inlineRadio1" value="man" {{ old('sex') == 'man' ? 'checked' : '' }}>
                                                    <label for="inlineRadio1"> 男性 </label>
                                                </div>

                                                <div class="radio radio-info radio-inline">
                                                    <input type="radio" name="sex" id="inlineRadio2" value="woman" {{ old('sex') == 'man' ? 'checked' : '' }}>
                                                    <label for="inlineRadio2"> 女性 </label>
                                                </div>
                                                {!! ($errors->has('sex') ? $errors->first('sex', '<p class="text-danger">:message</p>') : '') !!}
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-3 control-label">氏名<div id="re0">※</div></label>
                                            <div class="col-md-7 name_input_box">
                                                <div class="col-md-6">
                                                    <input id="first_name" type="text" name="first_name" class="form-control" data-parsley-length="[1,10]" placeholder="姓" value="{{ old('first_name') }}">
                                                    {!! ($errors->has('first_name') ? $errors->first('first_name', '<p class="text-danger">:message</p>') : '') !!}
                                                </div>
                                                <div class="col-md-6">
                                                    <input id="last_name" type="text" name="last_name" class="form-control" data-parsley-length="[1,10]" placeholder="名" value="{{ old('last_name') }}">
                                                    {!! ($errors->has('last_name') ? $errors->first('last_name', '<p class="text-danger">:message</p>') : '') !!}
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group birthday">
                                            <label class="col-md-3 control-label">生年月日</label>
                                            <div class="col-md-7">
                                                <div class="col-md-4">
                                                    <select  class="selectpicker year" data-live-search="false" data-style="btn-white">
                                                        <option>年</option>
                                                        <?php
                                                        for ($i = 1900; $i <= 2017; $i++) {
                                                            echo '<option>' . $i . '</option>';
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                                <div class="col-md-4">
                                                    <select class="selectpicker month" data-live-search="false" data-style="btn-white">
                                                        <option>月</option>
                                                        <?php for ($i = 1; $i <= 12; $i++) {
                                                            echo '<option>' . $i . '</option>';
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                                <div class="col-md-4">
                                                    <select  class="selectpicker day" data-live-search="false" data-style="btn-white">
                                                        <option>日</option>
                                                        <?php for ($i = 1; $i <= 31; $i++) {
                                                            echo '<option>' . $i . '</option>';
                                                        }
                                                        ?>
                                                    </select>
                                                </div>
                                                <input type="hidden" name="birthday" value="{{ old('birthday') }}">
                                                {!! ($errors->has('birthday') ? $errors->first('birthday', '<p class="text-danger">:message</p>') : '') !!}
                                            </div>


                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-3 control-label">住所</label>
                                            <div class="col-md-7">
                                                <input type="text" name="location" id="location" class="form-control" placeholder="〇〇県....XX-XX 〇〇〇タワー8F 802号室" value="{{ old('location') }}">
                                                {!! ($errors->has('location') ? $errors->first('location', '<p class="text-danger">:message</p>') : '') !!}
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-3 control-label">電話番号</label>
                                            <div class="col-md-7">
                                                <input type="text" name="phone_number" id="phone_number" class="form-control" placeholder="000-0000-0000" value="{{ old('phone_number') }}">
                                                {!! ($errors->has('phone_number') ? $errors->first('phone_number', '<p class="text-danger">:message</p>') : '') !!}
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-3 control-label">所属会社名</label>
                                            <div class="col-md-7">
                                                <input type="text" name="company" id="company" class="form-control" placeholder="◯◯◯◯株式会社" value="">
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-3 control-label">職業<div>※</div></label>
                                            <div class="col-md-7">
                                                <select name="work" id="work" class="selectpicker" data-live-search="false" data-style="btn-white">
                                                    <option>選択されていません</option>
                                                    <option>会社員</option>
                                                    <option>公務員</option>
                                                    <option>自営業</option>
                                                    <option>自由業</option>
                                                    <option>経営者・役員</option>
                                                    <option>専業主婦</option>
                                                    <option>パート・アルバイト</option>
                                                    <option>学生</option>
                                                    <option>その他</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-3 control-label">Facebook</label>
                                            <div class="col-md-7">
                                                <input parsley-type="url" type="url" name="facebook_url" id="facebook_url" class="form-control" placeholder="自身のフェイスブックのURL" value="">
                                            </div>
                                        </div>

                                        <hr>

                                        <div class="form-group">
                                            <label class="col-md-3 control-label">配信ジャンル(予定で可)<div>※</div></label>
                                            <div class="col-md-7">
                                                <div class="checkbox">
                                                    <input id="category0" name="ca0" type="checkbox" value="category0">
                                                    <label for="checkbox0">
                                                        WEB
                                                    </label>
                                                </div>

                                                <div class="checkbox">
                                                    <input id="category1" name="ca1" type="checkbox" value="category1">
                                                    <label for="checkbox1">
                                                        プログラミング
                                                    </label>
                                                </div>

                                                <div class="checkbox">
                                                    <input id="category2" name="ca2" type="checkbox" value="category2">
                                                    <label for="checkbox2">
                                                        ビジネス
                                                    </label>
                                                </div>

                                                <div class="checkbox">
                                                    <input id="category3" name="ca3" type="checkbox" value="category3">
                                                    <label for="checkbox3">
                                                        ライフハック・自己啓発
                                                    </label>
                                                </div>

                                                <div class="checkbox">
                                                    <input id="category4" name="ca4" type="checkbox" value="category4">
                                                    <label for="checkbox4">
                                                        語学
                                                    </label>
                                                </div>

                                                <div class="checkbox">
                                                    <input id="category5" name="ca5" type="checkbox" value="category5">
                                                    <label for="checkbox5">
                                                        写真・映像
                                                    </label>
                                                </div>

                                                <div class="checkbox">
                                                    <input id="category6" name="ca6" type="checkbox" value="category6">
                                                    <label for="checkbox6">
                                                        料理
                                                    </label>
                                                </div>

                                                <div class="checkbox">
                                                    <input id="category7" name="ca7" type="checkbox" value="category7">
                                                    <label for="checkbox7">
                                                        アート・デザイン
                                                    </label>
                                                </div>

                                                <div class="checkbox">
                                                    <input id="category8" name="ca8" type="checkbox" value="category8">
                                                    <label for="checkbox8">
                                                        ハンドメイド・DIY
                                                    </label>
                                                </div>

                                                <div class="checkbox">
                                                    <input id="category9" name="ca9" type="checkbox" value="category9">
                                                    <label for="checkbox9">
                                                        ビューティー・ヘルス
                                                    </label>
                                                </div>

                                                <div class="checkbox">
                                                    <input id="category10" name="ca10" type="checkbox" value="category10">
                                                    <label for="checkbox10">
                                                        その他
                                                    </label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-3 control-label">配信内容(予定で可)<div>※</div></label>
                                            <div class="col-md-7">
                                                <textarea class="form-control" rows="5" placeholder=""></textarea>
                                            </div>
                                        </div>

                                        <hr>

                                        <br><br>

                                        <div class="form-group m-b-0">
                                            <label class="col-sm-4 control-label"></label>
                                            <div class="col-sm-5">
                                                <button type="submit" id="entry" class="btn btn-primary">
                                                    お申込みへ
                                                </button>
                                            </div>
                                        </div>

                                        <input type="hidden" name="service_plan"  value="premium">
                                        <input name="_token" value="{{ csrf_token() }}" type="hidden">
                                    </form>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>

            </div>

        </div>
    </div>
</div>
@stop
@extends('centaur.layout')

@section('title', 'ログイン')

@section('content')
<div class="account-pages"></div>
<div class="clearfix"></div>

<div class="wrapper-page">
    <div class="card-box">
        <div class="panel-heading">
            <h3 class="text-center"> <strong class="text-custom">ガッコウ＋</strong>へログイン</h3>
        </div>

        <div class="panel-body">
            <form class="form-horizontal m-t-20" role="form" method="post" action="{{ route('auth.login.attempt') }}">
                <div class="form-group {{ ($errors->has('email')) ? 'has-error' : '' }}">
                    <div class="col-xs-12">
                        <input class="form-control" placeholder="E-mail" name="email" type="text" value="{{ old('email') }}">
                        {!! ($errors->has('email') ? $errors->first('email', '<p class="text-danger">:message</p>') : '') !!}
                    </div>
                </div>

                <div class="form-group  {{ ($errors->has('password')) ? 'has-error' : '' }}">
                    <div class="col-xs-12">
                        <input class="form-control" placeholder="Password" name="password" type="password" value="">
                        {!! ($errors->has('password') ? $errors->first('password', '<p class="text-danger">:message</p>') : '') !!}
                    </div>
                </div>
                <input name="_token" value="{{ csrf_token() }}" type="hidden">

                <div class="form-group ">
                    <div class="col-xs-12">
                        <div class="checkbox checkbox-primary">
                            <input name="remember" type="checkbox" value="true" {{ old('remember') == 'true' ? 'checked' : ''}}>
                            <label for="checkbox-signup"> 次回からIDの入力を省略 </label>
                        </div>
                    </div>
                </div>

                <div class="form-group text-center m-t-40">
                    <div class="col-xs-12">
                        <input class="btn btn-lg btn-primary btn-block" type="submit" value="Login">
                    </div>
                </div>

                <div class="form-group m-t-20 m-b-0">
                    <div class="col-sm-12">
                        <a href="{{ route('auth.password.request.form') }}" class="text-dark">
                            <i class="fa fa-lock m-r-5"></i> 会員ID・パスワードをお忘れの方はこちら</a>
                    </div>
                </div>

                <div class="form-group m-t-20 m-b-0">
                    <div class="col-sm-12 text-center">
                        <h4><b>Sign in with</b></h4>
                    </div>
                </div>

                <div class="form-group m-b-0 text-center">
                    <div class="col-sm-12">
                        <button type="button" class="btn btn-facebook waves-effect waves-light m-t-20">
                            <i class="fa fa-facebook m-r-5"></i> Facebook
                        </button>

                        <button type="button" class="btn btn-twitter waves-effect waves-light m-t-20">
                            <i class="fa fa-twitter m-r-5"></i> Twitter
                        </button>

                        <button type="button" class="btn btn-googleplus waves-effect waves-light m-t-20">
                            <i class="fa fa-google-plus m-r-5"></i> Google+
                        </button>
                    </div>
                </div>
            </form>

        </div>
    </div>
    <div class="row">
        <div class="col-sm-12 text-center">
            <p>
                会員でない方? <a href="{{ route('auth.register.form') }}" class="text-primary m-l-5"><b>会員登録</b></a>
            </p>
        </div>
    </div>

</div>

<script>
    var resizefunc = [];
</script>
@stop
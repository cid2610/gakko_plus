@extends('layout')
@section('content')
<div class="wraper container">
    <!-- Page-Title -->
    <div class="row">
        <div class="col-sm-12">
            <div class="page-header-2">
                <h4 class="page-title">授業の投稿</h4>
                <ol class="breadcrumb">
                    <li class="active">
                        IT、ビジネス、学問、料理...などなど、自分の好きな事や得意を配信してみよう！
                    </li>
                </ol>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-12">
            <div class="card-box">
                <div class="row">

                    <!-- Left <div class="col-lg-6"> -->
                    <div class="col-lg-12">
                        <h5><b></b></h5>
                        <p class="text-muted font-13 m-b-30">
                            １．講座を選択して下さい。(まだ作成されていない場合『講座・授業の一覧』より作成可能です。)
                            <br>２．授業名を入力して下さい。
                            <br>３．必要に応じてチャプター(動画の流れ)を入力して下さい。必須項目ではありません。
                            <br>４．動画ファイルを選択し、アップロードを完了して下さい。
                        </p>
                        @if (count($errors) > 0)
                            <div class="alert alert-danger">
                                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                <ul>
                                    @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                    @endforeach
                                </ul>
                            </div>
                        @endif
                        {!! Form::open(array('route' => 'lessons.store','method'=>'POST','files' => true, 'class' => "form-horizontal group-border-dashed")) !!}
                            <div class="form-group">
                                <label class="col-sm-3 control-label">講座名</label>
                                <div class="col-sm-6">
                                    <select name="course" id="create" class="selectpicker" data-live-search="true" data-style="btn-white">
                                        <option>選択して下さい</option>
                                        <option value="new">＜講座を新規作成する＞</option>
                                        @foreach($courses as $id=>$title)
                                            <option value="{{ $id }}"> {{ $title }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <hr>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">授業名</label>
                                <div class="col-sm-6">
                                    <input name="title" id="title" type="text" class="form-control" required="" placeholder="例：開発環境の構築">
                                </div>
                            </div>
                            <hr>
                            @for($i = 1; $i <= 10; $i++)
                            <div class="form-group">
                                <label class="control-label col-sm-3">チャプター {{ $i }}</label>
                                <div class="col-sm-6">
                                    <div class="input-group" id="date-range">
                                        <input id="name{{ $i }}" type="text" class="form-control" name="start[]" placeholder="チャプター名"/>
                                        <span class="input-group-addon bg-custom b-0 text-white">to</span>
                                        <input id="chapter{{ $i }}" type="text" class="form-control" name="end[]" />
                                    </div>
                                </div>
                            </div>
                            @endfor
                            <hr>

                            <div class="form-group">
                                <label class="col-sm-3 control-label">動画ファイル</label>
                                <div class="col-sm-6">
                                   <span class="btn btn-success fileinput-button">
                                        <input id="uploadMovie" type="file" name="file" accept='video/*' data-url="{{ route('upload.video', ['type' => 'lessons']) }}">
                                        {{--<i class="glyphicon glyphicon-plus"></i>--}}
                                   </span>
                                    <br> <br>
                                    <!-- The global progress bar -->
                                    <div id="status"></div>
                                    <div id="progress" class="progress">
                                        <div class="progress-bar progress-bar-success"></div>
                                    </div>

                                    <div class="panel panel-default">
                                        <div class="panel-heading">
                                            <h3 class="panel-title">アップロードの際の注意</h3>
                                        </div>
                                        <div class="panel-body">
                                            <ul>
                                                <li>ファイルが選択されると自動的にアップロードが開始されます。</li>
                                                <br>
                                                <li>サポートしているファイル形式：.MOV、.MPEG4、MP4、.AVI、.WMV、.MPEGPS、.FLV </li>
                                                <li>目安：長さ～1時間、容量～2GB</li>
                                                <li>※事前にエンコードを推奨しています。容量の削減にご協力下さい。</li>
                                            </ul>
                                        </div>
                                        <div>
                                            <video id="video" type="video/mp4" controls width="512"  height="384" ></video>
                                            <button type="button" id="capture" class="btn btn-success">画像作成</button>
                                            <input type="hidden" id="thumbnail" name="thumbnail" >
                                            <input type="hidden" id="asset_id" name="asset_id" >
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-3 control-label">動画ファイル</label>
                                <div class="col-sm-6">
                                    <canvas id="canvas" width="512" height="384" style="border:solid 1px #dddddd;" ></canvas> <br/><br/>
                                </div>
                            </div>

                            <div class="form-group m-b-0">
                                <div class="col-sm-offset-3 col-sm-9 m-t-15">
                                    <button type="submit" class="btn btn-primary" id="lessonup">
                                        投稿申請
                                    </button>
                                    <br><br>
                                    <p>※フォームの入力と動画ファイルのアップロードが完了しましたら投稿申請ボタンを押して下さい。
                                        <br>審査完了後に公開されます。審査は基本的に翌日中に終わる予定です。</p>
                                </div>
                            </div>

                        {!! Form::close() !!}
                    </div>
                    <!-- Left <div class="col-lg-6"> -->

                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('script')
    <script src="{{ asset('/js/pages/create_lesson.js') }}"></script>
@stop
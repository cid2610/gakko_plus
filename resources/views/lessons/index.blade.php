@extends('layout')
@section('content')
    <div class="row">
        <div class="col-sm-6">
            {{--<div class="btn-group pull-right m-t-15">--}}
            {{--<button type="button" class="btn btn-default dropdown-toggle waves-effect waves-light" data-toggle="dropdown" aria-expanded="false">講座作成 --}}
            {{--<span class="m-l-5"><i class="fa fa-cog"></i></span>--}}
            {{--</button>--}}
            {{--<ul class="dropdown-menu drop-menu-right" role="menu">--}}
            {{--<li><a href="#">Action</a></li>--}}
            {{--<li><a href="#">Another action</a></li>--}}
            {{--<li><a href="#">Something else here</a></li>--}}
            {{--<li class="divider"></li>--}}
            {{--<li><a href="#">Separated link</a></li>--}}
            {{--</ul>--}}
            {{--</div>--}}

            <h4 class="page-title">マイ授業</h4>
            <ol class="breadcrumb">
                <li><a href="{{ route('top') }}">ホーム</a></li>
                <li class="active">マイ授業</li>
            </ol>
        </div>
        @php ($user = Sentinel::getUser())
        @if ($user->service_plan == 'premium')
        <div class="btn-group pull-right m-t-15">
            <a href="{{route('lessons.create')}}" class="btn btn-sm btn-primary waves-effect waves-light">授業投稿<span class="m-l-5"><i class="fa fa-cog"></i></span>
            </a>
        </div>
        @endif
    </div>

    <div class="row port">
        <div class="portfolioContainer" style="position: relative; height: 1176px;">
            @foreach ($lessons as $lesson)
                <div class="col-sm-6 col-lg-3 col-md-4 webdesign illustrator">
                    <div class="gal-detail thumb">
                        <a href="{{ route('lessons.show', ['id' => $lesson->id]) }}" class="image-popup" title="Screenshot-1">
                            <img src="{{ $lesson->thumbnail ? asset('images/lessons/'. $lesson->thumbnail) : asset('images/courses/no-image.jpg') }}" class="thumb-img" alt="work-thumbnail">
                        </a>
                        <h4>{{ $lesson->title }}</h4>
                        講座：<a href="{{ route('courses.show', $lesson->course_id) }}">{{ $lesson->course_title }}</a>
                    </div>
                </div>
            @endforeach
        </div>
        <div class="col-xs-12" >
            <div class="pull-right">
                {!! $lessons->render() !!}
            </div>
        </div>
    </div>

@stop


@extends('layout')
@section('content')
    <div class="wraper container">
        <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <div class="page-header-2">
                    <h4 class="page-title">授業の投稿</h4>
                    <ol class="breadcrumb">
                        <li class="active">
                            IT、ビジネス、学問、料理...などなど、自分の好きな事や得意を配信してみよう！
                        </li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="card-box">
                    <div class="row">

                        <!-- Left <div class="col-lg-6"> -->
                        <div class="col-lg-12">
                            <h5><b></b></h5>
                            <p class="text-muted font-13 m-b-30">
                                １．講座を選択して下さい。(まだ作成されていない場合『講座・授業の一覧』より作成可能です。)
                                <br>２．授業名を入力して下さい。
                                <br>３．必要に応じてチャプター(動画の流れ)を入力して下さい。必須項目ではありません。
                                <br>４．動画ファイルを選択し、アップロードを完了して下さい。
                            </p>
                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            {!! Form::model($lesson, ['method' => 'PATCH','files' => true,'route' => ['lessons.update', $lesson->id],'class' => "form-horizontal group-border-dashed"]) !!}

                            <div class="form-group">
                                <label class="col-sm-3 control-label">講座名</label>
                                <div class="col-sm-6">
                                    <select name="create" id="create" class="selectpicker" data-live-search="true" data-style="btn-white">
                                        <option>選択して下さい</option>
                                        <option>＜講座を新規作成する＞</option>
                                        <?php
                                        $course_data = [];
                                        foreach ($course_data as $value) {
                                            if ($value["id"] == "0") {
                                                echo '<option>' . $value['title'] . '</option>';
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>

                            <hr>

                            <div class="form-group">
                                <label class="col-sm-3 control-label">授業名</label>
                                <div class="col-sm-6">
                                    {!! Form::text('title', null, array('placeholder' => '例：開発環境の構築','class' => 'form-control')) !!}
                                </div>
                            </div>

                            <hr>

                            <?php
                            for($i = 1; $i < 11; $i++){
                            echo '<div class="form-group">';
                            echo '  <label class="control-label col-sm-3">チャプター'.$i.'</label>';
                            echo '  <div class="col-sm-6">';
                            echo '      <div class="input-group" id="date-range">';
                            echo '          <input id="name'.$i.'" type="text" class="form-control" name="start[]" value ="'. $chapter_name[$i-1] .'" placeholder="チャプター名"/>';
                            echo '          <span class="input-group-addon bg-custom b-0 text-white">to</span>';
                            echo '          <input id="chapter'.$i.'" type="text" class="form-control" value ="'. $chapter_time[$i-1] .'" name="end[]" />';
                            echo '      </div>';
                            echo '  </div>';
                            echo '</div>';
                        }
                        ?>
                        <hr>

                        <div class="form-group">
                            <label class="col-sm-3 control-label">動画ファイル</label>
                            <div class="col-sm-6">
                                           <span class="btn btn-success fileinput-button">
                                                            <i class="glyphicon glyphicon-plus"></i>
                                                            <span>SELECT</span>
                                                            <input id="fileupload" type="file" name="files[]" accept='video/*' multiple>
                                            </span>
                                <br>
                                <br>
                                <div id="progress" class="progress">
                                    <div class="progress-bar progress-bar-success"></div>
                                </div>

                                <div id="files" class="files"></div>

                                <div class="panel panel-default">
                                    <div class="panel-heading">
                                        <h3 class="panel-title">アップロードの際の注意</h3>
                                    </div>
                                    <div class="panel-body">
                                        <ul>
                                            <li>ファイルが選択されると自動的にアップロードが開始されます。</li>
                                            <br>
                                            <li>サポートしているファイル形式：.MOV、.MPEG4、MP4、.AVI、.WMV、.MPEGPS、.FLV </li>
                                            <li>目安：長さ～1時間、容量～2GB</li>
                                            <li>※事前にエンコードを推奨しています。容量の削減にご協力下さい。</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="form-group m-b-0">
                            <div class="col-sm-offset-3 col-sm-9 m-t-15">
                                <button type="submit" class="btn btn-primary" id="lessonup">
                                    投稿申請
                                </button>
                                <br><br>
                                <p>※フォームの入力と動画ファイルのアップロードが完了しましたら投稿申請ボタンを押して下さい。
                                    <br>審査完了後に公開されます。審査は基本的に翌日中に終わる予定です。</p>
                            </div>
                        </div>

{!! Form::close() !!}
</div>
<!-- Left <div class="col-lg-6"> -->

</div>
</div>
</div>
</div>
</div>

@endsection

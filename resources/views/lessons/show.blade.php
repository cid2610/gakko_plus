@extends('layout')
@section('css')
    <link href="//amp.azure.net/libs/amp/latest/skins/amp-default/azuremediaplayer.min.css" rel="stylesheet">
@stop

@section('script')
    <script src="//amp.azure.net/libs/amp/latest/azuremediaplayer.min.js"></script>
    <script src="{{ asset('/js/pages/show_lesson.js')}}"></script>
@stop

@section('content')
    <div class="row">
        <div class="col-md-9 col-md-push-3" style="margin-top: 20px">
            <section>
                <div id="videoContainer" style="overflow: hidden; height: 500px;" data-url = "{{ $lesson->streaming_url }}">
                    {{--<video id="vid1" class="azuremediaplayer amp-default-skin" autoplay controls width="640" height="400" poster="poster.jpg" data-setup='{"nativeControlsForTouch": false}'>--}}
                        {{--<source src="http://amssamples.streaming.mediaservices.windows.net/91492735-c523-432b-ba01-faba6c2206a2/AzureMediaServicesPromo.ism/manifest" type="application/vnd.ms-sstr+xml" />--}}
                        {{--<p class="amp-no-js">--}}
                            {{--To view this video please enable JavaScript, and consider upgrading to a web browser that supports HTML5 video--}}
                        {{--</p>--}}
                    {{--</video>--}}
                </div>
            </section>
        </div>

        <div class="col-md-3 col-md-pull-9 well" style="font-size:0.95em">
            {{--<h3 style="margin-top:0">{{ $lesson->title }}</h3>--}}
            {{--<p>--}}
                {{--講座：--}}
                {{--<br>--}}
                {{--<a href="{{ route('courses.show', ['id' => $lesson->course_id ]) }}" class="btn btn-info" target="_blank">{{ $lesson->course_title }}</a>--}}
            {{--</p>--}}
            {{--<hr>--}}

            <div class="card-box">
                <h4 class="m-t-0 m-b-20 header-title"><b>{{ $lesson->title }}</b></h4>
                <hr>
                <ul class="list-unstyled transaction-list">
                    @foreach($chapterName as $i=>$name)
                        @if($name)
                            <li>
                                <i class="text-success"></i>
                                <span class="tran-text"> {{ $name }}</span>
                                <span class="text-muted pull-right">{{ isset($chapterTime[$i]) ? $chapterTime[$i]: '' }}</span>
                                <span class="clearfix"></span>
                            </li>
                        @endif
                    @endforeach
                </ul>
            </div>
        </div>
        <div class="col-xs-12">

        </div>
    </div>

@stop


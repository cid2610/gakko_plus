@extends('layout')

@section('title', 'トップ')
@section('content')
<!-- Start content -->
<div class="content">
    <div class="container top_page">
        <!--
            スライダー
        -->
        <ul class="slider center-item">
            <li><a href="description.php"><img class="slider_image_07 slider_image" src="{{ asset('images/webcampus_banner_07.png') }}"></a></li>
            <li><a href="https://gakko-plus.com/guide.php"><img class="slider_image_08 slider_image" src="{{ asset('images/webcampus_banner_08.png') }}"></a></li>
            <li><a href="https://gakko-plus.com/lecturer-app.php"><img class="slider_image_09 slider_image" src="{{ asset('images/webcampus_banner_09.png') }}"></a></li>
        </ul>
        <!--
            ライブ放送のスケジュール
        -->
        <div class="row top_content">
            <div class="col-sm-12">
                <h4 class="page-header header-title">ライブ放送のスケジュール</h4>
            </div>
        </div>

        <div class="row top_content">
            <ul class="slider center-item-content top_live_date_content">
                {{--@foreach($courses as $item)--}}
                {{--<li>--}}
                    {{--<div class="card-box widget-box-1 bg-white top_live_date_box">--}}
                        {{--<div class="image top_live_date_box_in">--}}
                            {{--<!-- <i class="fa fa-heart-o text-muted pull-right inform" data-toggle="tooltip" data-placement="bottom" title="" data-original-title="動画の説明もしくはお気に入り登録"></i> -->--}}
                            {{--<img class="img-rounded cover" width="100%" src="{{ route('courses.show', ['id' => $item->id]) }}">--}}
                            {{--<p>2017年3月10日 20:00〜</p>--}}
                        {{--</div>--}}
                        {{--<h4 class="text-dark">動画タイトル</h4>--}}
                    {{--</div>--}}
                {{--</li>--}}
                {{--@endforeach--}}
            </ul>
        </div>


        <!--  新着の授業動画  -->
        <div class="row top_content">
            <div class="col-sm-12">
                <h4 class="page-header  header-title">新着の授業動画</h4>
            </div>
        </div>

        <div class="row top_content">
            <ul class="slider center-item-content">
                @foreach($lessons as $item)
                <li>
                    <a href="{{ route('lessons.show', ['id' => $item->id ]) }}">
                        <div class="card-box widget-box-1 bg-white new_movie_slider_box">
                        <div class="image">
                            <h2 class="text-primary text-center new_movie_slider_image">
                            <img class="img-rounded cover slider_img" width="100%" src="{{ $item->thumbnail ? asset('images/lessons/'. $item->thumbnail) : asset('images/courses/no-image.jpg') }}">
                            </h2>
                        </div>
                        <p class="text-muted new_movie_slider_time">
                            <span class="pull-right"><i class="fa ti-time text-primary m-r-5"></i>24:20 </span>
                        </p>
                        <h4 class="text-dark slider_movie_title">{{ $item->title }}</h4></div></a>
                </li>
                @endforeach
           </ul>
        </div>

        <!--
            再生回数の多い先生
        -->
        <div class="row top_content">
            <div class="col-sm-12">
                <h4 class="page-header  header-title">再生回数の多い先生</h4>
            </div>
        </div>
        <div class="row top_content">

            <!--
                <br><p class="text-center">準備中 ＜集計中＞</P>
            -->
            <ul class="slider center-item-content_teacher famous-teacher">
                @foreach($premiumUsers as $premiumUser)
                    <li>
                        <div class="card-box p-b-0 famous_teacher_box">
                            <a href="{{ route('profile', ['id' => $premiumUser->id ]) }}" class="center-block text-center text-dark">
                                <img src="{{ asset('images/avatar-1.jpg') }}" class="thumb-lg img-thumbnail img-circle" alt="">
                                <div class="h5 m-b-0 famous_teacher_name">
                                    <strong>{{ $premiumUser->first_name . $premiumUser->last_name }}</strong>
                                </div>
                            </a>
                            <p class="teacher_job">{{ $premiumUser->company }}</p>
                        </div>
                    </li>
                @endforeach
            </ul>

        </div>
        <!--
            ページ下記
        -->
        <div class="row top_content">
            <div class="col-sm-12">
                <h4 class="page-header  header-title">ニュース</h4>
            </div>
        </div>

        <div class="row news_content">
            <div class="col-lg-7 news_content_box">

            </div>

            <div class="col-lg-5 sns_content_box">
                <div class="card-box">
                    <h4 class="text-dark header-title m-t-0 m-b-30">SNS</h4>
                    <div class="fb-page" data-href="https://www.facebook.com/gakkoplus/" data-tabs="timeline" data-width="500" data-height="204" data-small-header="true" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/gakkoplus/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/gakkoplus/">ガッコウ＋</a></blockquote></div>

                    <p class="center-text"><iframe src="https://www.facebook.com/plugins/page.php?href=https%3A%2F%2Fwww.facebook.com%2Fgakkoplus%2F&tabs=timeline&width=300&height=200&small_header=true&adapt_container_width=true&hide_cover=true&show_facepile=false&appId"
                        width="100%" height="200" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe></p>

                </div>
            </div>
        </div>
        <!-- end row -->

    </div>
</div>
<!-- end col -->
@stop
@extends('layout')

@section('title', 'トップ')
@section('content')
<div class="content-page">
    <!-- Start content -->
    <div class="content">
        <div class="container">
            <!-- Page-Title -->
            <div class="row">
                <div class="col-sm-12">
                    <h4 class="page-title">検索結果</h4>
                </div>
            </div>

            {!! Form::open(['method'=>'GET','url'=>'search','class'=>'','role'=>'search'])  !!}
            <div class="row">
                <div class="col-md-8 col-md-offset-2">
                    <div class="input-group m-t-10">
                        <input type="text" id="search" name="keyword" class="form-control input-lg" placeholder="">
                                    <span class="input-group-btn">
                                    <button type="submit" class="btn waves-effect waves-light btn-default btn-lg"><i class="fa fa-search m-r-5"></i> 検索</button>
                                    </span>
                    </div>
                </div>
            </div>
            {!! Form::close() !!}
            <div class="row">
                <div class="col-lg-12">
                    <div class="search-result-box m-t-40">
                        <ul class="nav nav-tabs">
                            <li class="active">
                                <a href="#home" data-toggle="tab" aria-expanded="true">
                                    <span class="visible-xs"><i class="fa fa-home"></i></span>
                                    <span class="hidden-xs"><b>講座</b> <span class="badge badge-primary m-l-10">{{ count($courses) }}</span></span>
                                </a>
                            </li>
                            <li class="">
                                <a href="#users" data-toggle="tab" aria-expanded="false">
                                    <span class="visible-xs"><i class="fa fa-user"></i></span>
                                    <span class="hidden-xs"><b>講師</b> <span class="badge badge-pink m-l-10">{{ count($lessons) }}</span> </span>
                                </a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="home">
                                <div class="row">
                                    <div class="col-md-12">
                                        @foreach($courses as $item)
                                        <div class="search-item">
                                            <img src="{{ $item->image ? asset('images/courses/'. $item->image) : asset('images/courses/no-image.jpg') }}" class="lecture_thumbnail">
                                            <h3 class="h5 font-600 m-b-5"><a href="{{ route('courses.show', ['id' => $item->id]) }}">{{ $item->title }}</a></h3>
                                            <div class="font-13 text-success m-b-10">
                                                {{ $cat = '' }}
                                                @if ($item->category)
                                                    @foreach(explode(',',$item->category) as $row)
                                                        @php  $cat .= $categories[$row] . ',' @endphp
                                                    @endforeach
                                                @endif
                                                更新日：{{ $item->created_at->format('Y年m月d日') }}  | 担当講師：{{ isset($item->user_id) ? $item->users[0]->username : "" }} | カテゴリー：{{ rtrim($cat,',')}}
                                            </div>
                                            <p>
                                                {!!  nl2br($item->overview)  !!}
                                            </p>
                                        </div>
                                        @endforeach
                                        <ul class="pagination pagination-split pull-right">
                                            {!! $courses->render() !!}
                                        </ul>

                                        <div class="clearfix"></div>
                                    </div>
                                </div>
                            </div>

                            <!-- end All results tab -->

                            <!-- Users tab -->
                            <div class="tab-pane" id="users">

                                @foreach($lessons as $item)
                                <div class="search-item">
                                    <div class="media">
                                        <div class="media-left">
                                            <a href="#"> <img class="media-object img-circle" alt="64x64" src="{{ $item->thumbnail ? asset('images/lessons/'. $item->thumbnail) : asset('images/courses/no-image.jpg') }}" style="width: 64px; height: 64px;"> </a>
                                        </div>
                                        <div class="media-body">
                                            <h4 class="media-heading"><a href="{{ route('lessons.show', ['id' => $item->id]) }}">{{ $item->title }}</a></h4>
                                            <p>
                                                <b>Career</b>
                                                <br/>
                                                <span><a href="#" class="text-muted">{{  isset($item->user_id) ? $item->users[0]->job: "" }}</a></span></p>
                                            <p>
                                                <b>My profile</b>
                                                <br/>
                                                <span class="text-muted">{{ isset($item->user_id) ? $item->users[0]->my_message : "" }}</span>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                                <ul class="pagination pagination-split pull-right">
                                    {!! $lessons->render() !!}
                                </ul>
                                <div class="clearfix"></div>
                            </div>
                            <!-- end Users tab -->

                        </div>
                    </div>
                </div>
            </div>


        </div> <!-- container -->

    </div> <!-- content -->

</div>


@stop


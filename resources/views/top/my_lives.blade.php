@extends('layout')
@section('content')
    <div class="row">
        <div class="col-sm-12">
            <h4 class="page-title">
                生放送一覧
                <a class="btn btn-default pull-right" href="{{ route('lives.create') }}">生放送作成</a>
            </h4>
            <ol class="breadcrumb">
                <li><a href="{{ route('top') }}">ホーム</a></li>
                <li class="active">生放送一覧</li>
            </ol>
        </div>
    </div>

    <div class="row port">
        <div class="portfolioContainer" style="position: relative; height: 1176px;">
            @foreach ($lives as $live)
                <div class="col-sm-6 col-lg-3 col-md-4 webdesign illustrator">
                    <div class="gal-detail thumb">
                        <h5>開始: {{ $live->start_time }} </h5>
                        <h5>終了: {{ $live->end_time }}</h5>
                        <a href="{{ route('lives.show', ['id' => $live->id]) }}" class="image-popup" title="Screenshot-1">
                            <img src="{{ $live->image ? asset('images/lives/'. $live->image) : asset('images/lives/no-image.jpg') }}" class="thumb-img" alt="work-thumbnail" style="max-height: 80px;">
                        </a>
                        <h5>{{ $live->title }}</h5>
                    </div>
                </div>
            @endforeach
        </div>
        <div class="col-xs-12" >
            <div class="pull-right">
                {!! $lives->render() !!}
            </div>
        </div>
    </div>

@stop


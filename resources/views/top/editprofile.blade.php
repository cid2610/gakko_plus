@extends('layout')

@section('title', '会員登録')

@section('script')
    <script src="{{ asset('/js/pages/register.js') }}"></script>
@stop

@section('content')
<div class="account-pages"></div>
<div class="clearfix"></div>
<div class="content">

    <div class="wraper container">

        <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <div class="page-header-2">
                    <h4 class="page-title">個人設定</h4>
                    <ol class="breadcrumb">
                        <li class="active">
                            アカウントの情報やお支払いについて設定が可能です。
                        </li>
                    </ol>
                </div>
            </div>
        </div>



        <div class="row">
            <div class="col-sm-12">
                <div class="card-box">
                    <div class="row">
                        <div class="col-lg-12">
                            <h5><b></b></h5>
                            <p class="text-muted font-13 m-b-30">
                            </p>
                            {!! Form::model($profile, ['method' => 'POST','files' => true,'class'=>"form-horizontal group-border-dashed",'route' => ['updateprofile', $profile->id]]) !!}
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">プラン</label>
                                    <div class="col-sm-6">
                                        <input type="text" class="form-control" readonly="" value="<?php if ($profile->service_plan == 'upgrade') { echo 'アップグレード'; } else if ($profile->service_plan == 'Upgrade(Student)') { echo 'アップグレード(学割)'; } else if ($profile->service_plan == 'premium') { echo 'プレミアム'; } ?>" />
                                    </div>
                                </div>

                                <br><hr><br>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label">ユーザー名</label>
                                    <div class="col-sm-6">
                                        {!! Form::text('username', null, array('placeholder' => 'Username','class' => 'form-control','id' => "username")) !!}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">メールアドレス</label>
                                    <div class="col-sm-6">
                                        {!! Form::text('email', null, array('placeholder' => 'Email','class' => 'form-control','id' => "email")) !!}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">パスワード変更</label>
                                    <div class="col-sm-3">
                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                        <input type="password" id="password_" name="password" class="form-control" placeholder="現在の" />
                                        @if ($errors->has('password'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('password') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                    <div class="col-sm-3">
                                        <input type="password" id="password_retry_" name="password_retry" class="form-control" data-parsley-equalto="#pass2" placeholder="新規の" />
                                        @if ($errors->has('password_retry'))
                                            <span class="help-block">
                                                <strong>{{ $errors->first('password_retry') }}</strong>
                                            </span>
                                        @endif
                                    </div>
                                </div>

                                <br><hr><br>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label">性別</label>
                                    <div class="col-sm-6">
                                        <div class="radio radio-info radio-inline">
{{--                                            {!! Form::text('sex', 'man', array('placeholder' => 'sex','class' => 'form-control','id' => "inlineRadio1")) !!}--}}
                                            <input type="radio" name="sex" id="inlineRadio1" value="man" {{ old('sex') == 'man' ? 'checked' : '' }}>
                                            <label for="inlineRadio1"> 男性 </label>
                                        </div>

                                        <div class="radio radio-info radio-inline">
{{--                                            {!! Form::text('sex', 'woman', array('placeholder' => 'sex','class' => 'form-control','id' => "inlineRadio2")) !!}--}}
                                            <input type="radio" name="sex" id="inlineRadio2" value="woman" {{ old('sex') == 'man' ? 'checked' : '' }}>
                                            <label for="inlineRadio2"> 女性 </label>
                                        </div>
                                        {!! ($errors->has('sex') ? $errors->first('sex', '<p class="text-danger">:message</p>') : '') !!}

{{--                                        {!! Form::text('sex', null, array('placeholder' => 'sex','class' => 'form-control','id' => "sex")) !!}--}}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">氏名</label>
                                    <div class="col-sm-6">
                                        <div class="col-md-6">
                                            {!! Form::text('first_name', null, array('placeholder' => 'first_name','class' => 'form-control','id' => "first_name")) !!}
                                            {!! ($errors->has('first_name') ? $errors->first('first_name', '<p class="text-danger">:message</p>') : '') !!}
                                        </div>
                                        <div class="col-md-6">
                                            {!! Form::text('last_name', null, array('placeholder' => 'last_name','class' => 'form-control','id' => "last_name")) !!}
                                            {!! ($errors->has('last_name') ? $errors->first('last_name', '<p class="text-danger">:message</p>') : '') !!}
                                        </div>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label">生年月日</label>
                                    <div class="col-sm-6">
                                        <div class="col-md-4">
                                            <select  class="selectpicker year" data-live-search="false" data-style="btn-white">
                                                <option>年</option>
                                                <?php
                                                for ($i = 1900; $i <= 2017; $i++) {
                                                    echo '<option>' . $i . '</option>';
                                                }
                                                ?>
                                            </select>
                                        </div>
                                        <div class="col-md-4">
                                            <select class="selectpicker month" data-live-search="false" data-style="btn-white">
                                                <option>月</option>
                                                <?php for ($i = 1; $i <= 12; $i++) {
                                                    echo '<option>' . $i . '</option>';
                                                }
                                                ?>
                                            </select>
                                        </div>
                                        <div class="col-md-4">
                                            <select  class="selectpicker day" data-live-search="false" data-style="btn-white">
                                                <option>日</option>
                                                <?php for ($i = 1; $i <= 31; $i++) {
                                                    echo '<option>' . $i . '</option>';
                                                }
                                                ?>
                                            </select>
                                        </div>
                                        <input type="hidden" name="birthday" value="{{ old('birthday') }}">
                                        {!! ($errors->has('birthday') ? $errors->first('birthday', '<p class="text-danger">:message</p>') : '') !!}
{{--                                        {!! Form::text('birthday', null, array('placeholder' => 'birthday','class' => 'form-control','id' => "birthday")) !!}--}}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">住所</label>
                                    <div class="col-sm-6">
                                        {!! Form::text('location', null, array('placeholder' => '〇〇県....XX-XX 〇〇〇タワー8F 802号室','class' => 'form-control','id' => "location")) !!}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">電話番号</label>
                                    <div class="col-sm-6">
                                        {!! Form::text('phone_number', null, array('placeholder' => '000-0000-0000','class' => 'form-control','id' => "phone_number")) !!}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">職業</label>
                                    <div class="col-sm-6">
                                        {!! Form::text('job', null, array('placeholder' => 'エンジニア','class' => 'form-control','id' => "job")) !!}
                                    </div>
                                </div>

                                <br><hr><br>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Twitter</label>
                                    <div class="col-sm-6">
                                        {!! Form::text('link_twitter', null, array('placeholder' => 'https://...','class' => 'form-control','id' => "link_twitter",'parsley-type' => "url")) !!}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Facebook</label>
                                    <div class="col-sm-6">
                                        {!! Form::text('link_facebook', null, array('placeholder' => 'https://...','class' => 'form-control','id' => "link_facebook",'parsley-type' => "url")) !!}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">LinkedIn</label>
                                    <div class="col-sm-6">
                                        {!! Form::text('link_linkedin', null, array('placeholder' => 'https://...','class' => 'form-control','id' => "link_linkedin",'parsley-type' => "url")) !!}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">Google</label>
                                    <div class="col-sm-6">
                                        {!! Form::text('link_google', null, array('placeholder' => 'https://...','class' => 'form-control','id' => "link_google",'parsley-type' => "url")) !!}
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">自己紹介</label>
                                    <div class="col-sm-6">
                                        {!! Form::textarea('my_message', null, array('placeholder' => '最大1000文字','class' => 'form-control','id' => "my_message")) !!}
                                    </div>
                                </div>

                                <br><hr><br>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">オプション</label>
                                    <div class="col-sm-6">
                                        <div class="checkbox checkbox-pink">
                                            {{ Form::checkbox('mailoption',null,null, array('id'=>'mailoption','data-parsley-multiple' => "group1")) }}
                                            <label for="checkbox4"> ガッコウ＋からのメールを受け取る<br>新着講義の情報や、キャンペーン情報をお届けします</label>
                                        </div>
                                    </div>
                                </div>

                                <br><hr><br>

                                <div class="form-group">
                                    <label class="col-sm-3 control-label">クレジットカード番号(後ろ4個)</label>
                                    <div class="col-sm-6">
                                        <input parsley-type="url" type="url" id="card_no_4_" name="card_no_4" class="form-control" value="" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">有効期限</label>
                                    <div class="col-sm-6">
                                        <input parsley-type="url" type="url" id="card_kigen_" name="card_kigen" class="form-control" value="" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-3 control-label">カード名義人</label>
                                    <div class="col-sm-6">
                                        <input parsley-type="url" type="url" id="card_name_" name="card_name" class="form-control" value="" />
                                    </div>
                                </div>

                                <div class="form-group m-b-0">
                                    <div class="col-sm-offset-3 col-sm-9 m-t-15">
                                        <button type="submit" class="btn btn-primary" id="send">
                                            更新する
                                        </button>
                                    </div>
                                </div>

                            {!! Form::close() !!}
                        </div>

                    </div>
                </div>
            </div>
        </div>


    </div> <!-- container -->

</div> <!-- content -->
@stop
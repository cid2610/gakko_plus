@extends('layout')
@section('content')
    <div style="margin-top: 20px"></div>
    <div class="row">
        <div class="col-sm-12">
            <div class="btn-group pull-right m-t-15">
                <a href="{{route('editprofile')}}" class="btn btn-sm btn-primary waves-effect waves-light">設定<span class="m-l-5"><i class="fa fa-cog"></i></span>
                </a>
                {{--<ul class="dropdown-menu drop-menu-right" role="menu">--}}
                    {{--<li><a href="#">Action</a></li>--}}
                    {{--<li><a href="#">Another action</a></li>--}}
                    {{--<li><a href="#">Something else here</a></li>--}}
                    {{--<li class="divider"></li>--}}
                    {{--<li><a href="#">Separated link</a></li>--}}
                {{--</ul>--}}
            </div>
            <h4 class="page-title">プロファイル</h4>
            {{--<ol class="breadcrumb">--}}
                {{--<li><a href="#">Ubold</a></li>--}}
                {{--<li><a href="#">Extras</a></li>--}}
                {{--<li class="active">Profile</li>--}}
            {{--</ol>--}}
        </div>
    </div>

    <div class="row">
        <div class="col-md-4 col-lg-3">
            <div class="profile-detail card-box">
                <div>
                    <img src="assets/images/users/avatar-2.jpg" class="img-circle" alt="profile-image">

                    <ul class="list-inline status-list m-t-20">
                        <li>
                            <h3 class="text-primary m-b-5">{{ $user->follow or 0 }}</h3>
                            <p class="text-muted">Followings</p>
                        </li>

                        <li>
                            <h3 class="text-success m-b-5">{{ $user->follower or 0 }}</h3>
                            <p class="text-muted">Followers</p>
                        </li>
                    </ul>

                    <button type="button" class="btn btn-pink btn-custom btn-rounded waves-effect waves-light">Follow</button>

                    <hr>
                    <h4 class="text-uppercase font-600">マイメッセージ</h4>
                    <p class="text-muted font-13 m-b-30">
                        {{ $user->my_message }}
                    </p>

                    <div class="text-left">
                        <p class="text-muted font-13"><strong>名前 :</strong> <span class="m-l-15">{{ $user->first_name .  $user->last_name }}</span></p>

                        <p class="text-muted font-13"><strong>モバイル :</strong><span class="m-l-15">{{ $user->phone_number }}</span></p>

                        <p class="text-muted font-13"><strong>メール :</strong> <span class="m-l-15">{{ $user->email }}</span></p>

                        <p class="text-muted font-13"><strong>住所 :</strong> <span class="m-l-15">{{ $user->location }}</span></p>

                    </div>


                    <div class="button-list m-t-20">
                        <button type="button" class="btn btn-facebook waves-effect waves-light">
                            <i class="fa fa-facebook"></i>
                        </button>

                        <button type="button" class="btn btn-twitter waves-effect waves-light">
                            <i class="fa fa-twitter"></i>
                        </button>

                        <button type="button" class="btn btn-linkedin waves-effect waves-light">
                            <i class="fa fa-linkedin"></i>
                        </button>

                        <button type="button" class="btn btn-dribbble waves-effect waves-light">
                            <i class="fa fa-dribbble"></i>
                        </button>

                    </div>
                </div>

            </div>

            <div class="card-box">
                <h4 class="m-t-0 m-b-20 header-title"><b>Friends <span class="text-muted">(0)</span></b></h4>

                <div class="friend-list">
                    {{--<a href="#">--}}
                        {{--<img src="assets/images/users/avatar-1.jpg" class="img-circle thumb-md" alt="friend">--}}
                    {{--</a>--}}

                    {{--<a href="#">--}}
                        {{--<img src="assets/images/users/avatar-2.jpg" class="img-circle thumb-md" alt="friend">--}}
                    {{--</a>--}}

                    {{--<a href="#">--}}
                        {{--<img src="assets/images/users/avatar-3.jpg" class="img-circle thumb-md" alt="friend">--}}
                    {{--</a>--}}

                    {{--<a href="#">--}}
                        {{--<img src="assets/images/users/avatar-4.jpg" class="img-circle thumb-md" alt="friend">--}}
                    {{--</a>--}}

                    {{--<a href="#">--}}
                        {{--<img src="assets/images/users/avatar-5.jpg" class="img-circle thumb-md" alt="friend">--}}
                    {{--</a>--}}

                    {{--<a href="#">--}}
                        {{--<img src="assets/images/users/avatar-6.jpg" class="img-circle thumb-md" alt="friend">--}}
                    {{--</a>--}}

                    {{--<a href="#">--}}
                        {{--<img src="assets/images/users/avatar-7.jpg" class="img-circle thumb-md" alt="friend">--}}
                    {{--</a>--}}

                    {{--<a href="#" class="text-center">--}}
                        {{--<span class="extra-number">+89</span>--}}
                    {{--</a>--}}
                </div>
            </div>
        </div>


        <div class="col-lg-9 col-md-8">
            <form method="post" class="well">
                                    <span class="input-icon icon-right">
                                        <textarea rows="2" class="form-control" placeholder="Post a new message"></textarea>
                                    </span>
                <div class="p-t-10 pull-right">
                    <a class="btn btn-sm btn-primary waves-effect waves-light">送信</a>
                </div>
                <ul class="nav nav-pills profile-pills m-t-10">
                    <li>
                        <a href="#"><i class="fa fa-user"></i></a>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-location-arrow"></i></a>
                    </li>
                    <li>
                        <a href="#"><i class=" fa fa-camera"></i></a>
                    </li>
                    <li>
                        <a href="#"><i class="fa fa-smile-o"></i></a>
                    </li>
                </ul>

            </form>
            <div class="card-box">
                {{--<div class="comment">--}}
                    {{--<img src="assets/images/users/avatar-1.jpg" alt="" class="comment-avatar">--}}
                    {{--<div class="comment-body">--}}
                        {{--<div class="comment-text">--}}
                            {{--<div class="comment-header">--}}
                                {{--<a href="#" title="">Adam Jansen</a><span>about 2 minuts ago</span>--}}
                            {{--</div>--}}
                            {{--Story based around the idea of time lapse, animation to post soon!--}}

                            {{--<div class="m-t-15">--}}
                                {{--<a href="">--}}
                                    {{--<img src="assets/images/small/img1.jpg" class="thumb-md">--}}
                                {{--</a>--}}
                                {{--<a href="">--}}
                                    {{--<img src="assets/images/small/img2.jpg" class="thumb-md">--}}
                                {{--</a>--}}
                                {{--<a href="">--}}
                                    {{--<img src="assets/images/small/img3.jpg" class="thumb-md">--}}
                                {{--</a>--}}
                            {{--</div>--}}
                        {{--</div>--}}

                        {{--<div class="comment-footer">--}}
                            {{--<a href="#"><i class="fa fa-thumbs-o-up"></i></a>--}}
                            {{--<a href="#"><i class="fa fa-thumbs-o-down"></i></a>--}}
                            {{--<a href="#">Reply</a>--}}
                        {{--</div>--}}
                    {{--</div>--}}

                    {{--<div class="comment">--}}
                        {{--<img src="assets/images/users/avatar-2.jpg" alt="" class="comment-avatar">--}}
                        {{--<div class="comment-body">--}}
                            {{--<div class="comment-text">--}}
                                {{--<div class="comment-header">--}}
                                    {{--<a href="#" title="">John Smith</a><span>about 1 hour ago</span>--}}
                                {{--</div>--}}
                                {{--Wow impressive!--}}
                            {{--</div>--}}
                            {{--<div class="comment-footer">--}}
                                {{--<a href="#"><i class="fa fa-thumbs-o-up"></i></a>--}}
                                {{--<a href="#"><i class="fa fa-thumbs-o-down"></i></a>--}}
                                {{--<a href="#">Reply</a>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}

                    {{--<div class="comment">--}}
                        {{--<img src="assets/images/users/avatar-3.jpg" alt="" class="comment-avatar">--}}
                        {{--<div class="comment-body">--}}
                            {{--<div class="comment-text">--}}
                                {{--<div class="comment-header">--}}
                                    {{--<a href="#" title="">Matt--}}
                                        {{--Cheuvront</a><span>about 2 hours ago</span>--}}
                                {{--</div>--}}
                                {{--Wow, that is really nice.--}}
                            {{--</div>--}}
                            {{--<div class="comment-footer">--}}
                                {{--<a href="#"><i class="fa fa-thumbs-o-up"></i></a>--}}
                                {{--<a href="#"><i class="fa fa-thumbs-o-down"></i></a>--}}
                                {{--<a href="#">Reply</a>--}}
                            {{--</div>--}}
                        {{--</div>--}}

                        {{--<div class="comment">--}}
                            {{--<img src="assets/images/users/avatar-4.jpg" alt="" class="comment-avatar">--}}
                            {{--<div class="comment-body">--}}
                                {{--<div class="comment-text">--}}
                                    {{--<div class="comment-header">--}}
                                        {{--<a href="#" title="">Stephanie--}}
                                            {{--Walter</a><span>3 hours ago</span>--}}
                                    {{--</div>--}}
                                    {{--Nice work, makes me think of The Money Pit.--}}
                                {{--</div>--}}
                                {{--<div class="comment-footer">--}}
                                    {{--<a href="#"><i class="fa fa-thumbs-o-up"></i></a>--}}
                                    {{--<a href="#"><i class="fa fa-thumbs-o-down"></i></a>--}}
                                    {{--<a href="#">Reply</a>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="comment">--}}
                    {{--<img src="assets/images/users/avatar-1.jpg" alt="" class="comment-avatar">--}}
                    {{--<div class="comment-body">--}}
                        {{--<div class="comment-text">--}}
                            {{--<div class="comment-header">--}}
                                {{--<a href="#" title="">Kim Ryder</a><span>about 4 hours ago</span>--}}
                            {{--</div>--}}
                            {{--i'm in the middle of a timelapse animation myself! (Very different--}}
                            {{--though.) Awesome stuff.--}}
                        {{--</div>--}}
                        {{--<div class="comment-footer">--}}
                            {{--<a href="#"><i class="fa fa-thumbs-o-up"></i></a>--}}
                            {{--<a href="#"><i class="fa fa-thumbs-o-down"></i></a>--}}
                            {{--<a href="#">Reply</a>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="comment">--}}
                    {{--<img src="assets/images/users/avatar-7.jpg" alt="" class="comment-avatar">--}}
                    {{--<div class="comment-body">--}}
                        {{--<div class="comment-text">--}}
                            {{--<div class="comment-header">--}}
                                {{--<a href="#" title="">Nicolai Larson</a><span>10 hours ago</span>--}}
                            {{--</div>--}}
                            {{--the parallax is a little odd but O.o that house build is awesome!!--}}
                        {{--</div>--}}
                        {{--<div class="comment-footer">--}}
                            {{--<a href="#"><i class="fa fa-thumbs-o-up"></i></a>--}}
                            {{--<a href="#"><i class="fa fa-thumbs-o-down"></i></a>--}}
                            {{--<a href="#">Reply</a>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="comment">--}}
                    {{--<img src="assets/images/users/avatar-1.jpg" alt="" class="comment-avatar">--}}
                    {{--<div class="comment-body">--}}
                        {{--<div class="comment-text">--}}
                            {{--<div class="comment-header">--}}
                                {{--<a href="#" title="">Adam Jansen</a><span>about 2 minuts ago</span>--}}
                            {{--</div>--}}
                            {{--Story based around the idea of time lapse, animation to post soon!--}}

                            {{--<div class="m-t-15">--}}
                                {{--<a href="">--}}
                                    {{--<img src="assets/images/small/img1.jpg" class="thumb-md">--}}
                                {{--</a>--}}
                                {{--<a href="">--}}
                                    {{--<img src="assets/images/small/img2.jpg" class="thumb-md">--}}
                                {{--</a>--}}
                                {{--<a href="">--}}
                                    {{--<img src="assets/images/small/img3.jpg" class="thumb-md">--}}
                                {{--</a>--}}
                            {{--</div>--}}
                        {{--</div>--}}

                        {{--<div class="comment-footer">--}}
                            {{--<a href="#"><i class="fa fa-thumbs-o-up"></i></a>--}}
                            {{--<a href="#"><i class="fa fa-thumbs-o-down"></i></a>--}}
                            {{--<a href="#">Reply</a>--}}
                        {{--</div>--}}
                    {{--</div>--}}

                    {{--<div class="comment">--}}
                        {{--<img src="assets/images/users/avatar-2.jpg" alt="" class="comment-avatar">--}}
                        {{--<div class="comment-body">--}}
                            {{--<div class="comment-text">--}}
                                {{--<div class="comment-header">--}}
                                    {{--<a href="#" title="">John Smith</a><span>about 1 hour ago</span>--}}
                                {{--</div>--}}
                                {{--Wow impressive!--}}
                            {{--</div>--}}
                            {{--<div class="comment-footer">--}}
                                {{--<a href="#"><i class="fa fa-thumbs-o-up"></i></a>--}}
                                {{--<a href="#"><i class="fa fa-thumbs-o-down"></i></a>--}}
                                {{--<a href="#">Reply</a>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}

                {{--<div class="m-t-30 text-center">--}}
                    {{--<a href="" class="btn btn-default waves-effect waves-light btn-sm">Load More...</a>--}}
                {{--</div>--}}
            </div>
        </div>

    </div>


@stop
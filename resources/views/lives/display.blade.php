@extends('layout')
@section('css')
    <link href="//amp.azure.net/libs/amp/latest/skins/amp-default/azuremediaplayer.min.css" rel="stylesheet">
@stop

@section('script')
    <script src="//amp.azure.net/libs/amp/latest/azuremediaplayer.min.js"></script>
    <script src="{{ asset('/js/pages/show_lesson.js')}}"></script>
@stop

@section('content')
    <div class="row">

        <div class="card-box" style="overflow: auto">
            <div class="col-xs-12" style="margin-bottom: 10px">
                <h3 class="panel-title">
                    {{ $live->title }}
                </h3>
            </div>

            <div class="col-xs-8">
                <img src="{{ asset('/images/lives/' . $live->image) }}" width="400px" height="200px">
            </div>

            <div class="col-lg-4">
                <div class="panel panel-border panel-custom">
                    <div class="panel-heading">
                        <h3 class="panel-title">放送時間</h3>
                    </div>
                    <div class="panel-body">
                        <p>
                            開始時間: {{ $live->start_time }}
                        </p>
                        <p>
                            終了時間: {{ $live->end_time }}
                        </p>
                    </div>
                </div>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="col-xs-12">
            <div id="videoContainer" style="overflow: hidden; height: 500px;" data-url = "{{ $live->streaming_url }}">
            </div>
        </div>
    </div>
@stop


@extends('layout')

@section('content')
    <div class="card-box" style="overflow: auto">
        <div class="col-xs-12" style="margin-bottom: 10px">
            <h3 class="panel-title">
                {{ $live->title }}
            </h3>
            <div class="pull-right">
                @if ($live->publish)
                    <a href="{{route('lives.un_publish', ['id' => $live['id'] ])}}" class="btn btn-sm btn-danger waves-effect waves-light">非公開<span class="m-l-5"><i class="fa fa-cog"></i></span></a>
                @else
                    <a href="{{route('lives.publish', ['id' => $live['id'] ])}}" class="btn btn-sm btn-primary waves-effect waves-light">公開<span class="m-l-5"><i class="fa fa-cog"></i></span></a>
                @endif
            </div>
        </div>

        <div class="col-xs-8">
            <img src="{{ asset('/images/lives/' . $live->image) }}" width="400px" height="200px">
        </div>

        <div class="col-lg-4">
            <div class="panel panel-border panel-custom">
                <div class="panel-heading">
                    <h3 class="panel-title">放送時間</h3>
                </div>
                <div class="panel-body">
                    <p>
                        開始時間: {{ $live->start_time }}
                    </p>
                    <p>
                        終了時間: {{ $live->end_time }}
                    </p>
                </div>
            </div>
        </div>
        <div class="clearfix"></div>
        <br>
        <div class="col-xs-12">
            <h3 class="panel-title">放送URL</h3>
            <p>
                Live URL: {{ $live->ingest_url }}
            </p>
            <p>
                Preview URL: {{ $live->ingest_url }}
            </p>
            <p>
                Streaming URL: {{ $live->streaming_url }}
            </p>
            @if($live->publish)
                <a class="btn btn-primary" href="{{ route('lives.display', ['id' => $live->id ]) }}"> 確認する</a>
            @endif
        </div>
    </div>
@stop


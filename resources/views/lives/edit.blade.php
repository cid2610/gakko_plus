@extends('layout')
@section('content')
    <div class="wraper container">
        <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <div class="page-header-2">
                    <h4 class="page-title">講座・授業の管理</h4>
                    <ol class="breadcrumb">
                        <li class="active">
                            自身が投稿した講座・授業の作成から編集、また授業の公開・非公開設定等が可能です。
                        </li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="card-box">
                    <div class="row">
                        <div class="col-lg-12">
                            <h5><b></b></h5>
                            <p class="text-muted font-13 m-b-30">
                            </p>
                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif
                            {!! Form::model($course, ['method' => 'PATCH','files' => true,'class'=>"form-horizontal group-border-dashed",'route' => ['courses.update', $course->id]]) !!}

                            <div class="form-group has-feedback">
                                <label class="col-sm-3 control-label">講座名</label>
                                <div class="col-sm-7">
                                    {!! Form::text('title', null, array('placeholder' => 'Title','class' => 'form-control inputmask','id' => "title")) !!}
                                    <span class="glyphicon form-control-feedback"></span>
                                </div>
                            </div>

                            <div class="form-group has-feedback">
                                <label class="col-sm-3 control-label">講座概要</label>
                                <div class="col-sm-7">
                                    {!! Form::textarea('overview', null, array('placeholder' => 'Overview','class' => 'form-control inputmask','id' => "overview")) !!}
                                    <span class="glyphicon form-control-feedback"></span>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label">サムネイル</label>
                                <div class="col-sm-7">
                                    {!! Form::file('image', null, array('placeholder' => 'jpg,png','class' => 'form-control filestyle', 'data-icon'=>"false",'data-buttonname'=>"btn-white" ,'id'=>"ss")) !!}
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-3"></div>
                                <div id="course_imagebox" class="col-sm-7"></div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label">カテゴリー</label>
                                <div class="col-sm-7">
                                    @foreach ($categories as  $slug => $value)
                                        <div class="checkbox">
                                            <input tabindex="1" type="checkbox" name="category[]" id="category{{$slug}}"  {{ isset($course->category) && in_array($slug,  preg_split("/[\s,]+/", $course->category)) ? 'checked' : ''  }} value="{{$slug}}">
                                            <label for="checkbox{{$slug}}">
                                                {!! Form::label('category', $value) !!}
                                            </label>
                                        </div>
                                    @endforeach
                                </div>
                            </div>

                            <div class="form-group m-b-0">
                                <div class="col-sm-offset-3 col-sm-9 m-t-15">
                                    <button type="submit" class="btn btn-primary" id="btnsubmit">
                                        保存
                                    </button>
                                </div>
                            </div>
                            {!! Form::close() !!}

                            <br><hr><br>
                            <div class="row port">
                                <div class="portfolioContainer" style="position: relative;">
                                    @foreach ($lessons as $lesson)
                                        <div class="col-sm-6 col-lg-3 col-md-4 webdesign illustrator">
                                            <div class="gal-detail thumb">
                                                <a href="{{ route('lessons.show', ['id' => $lesson->id]) }}" class="image-popup" title="Screenshot-1">
                                                    <img src="{{ $lesson->thumbnail ? asset('images/lesson/'. $lesson->thumbnail) : asset('images/lesson/no-image.jpg') }}" class="thumb-img" alt="work-thumbnail">
                                                </a>
                                                <h4>{{ $lesson->title }}</h4>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

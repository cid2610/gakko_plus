@extends('layout')
@section('content')
    <div class="wraper container">
        <!-- Page-Title -->
        <div class="row">
            <div class="col-sm-12">
                <div class="page-header-2">
                    <h4 class="page-title">生放送作成</h4>
                    <ol class="breadcrumb">
                        <li class="active">
                            生放送から編集、また授業の公開・非公開設定等が可能です。
                        </li>
                    </ol>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <div class="card-box">
                    <div class="row">
                        <div class="col-lg-12">
                            <h5><b></b></h5>
                            <p class="text-muted font-13 m-b-30">
                            </p>
                            @if (count($errors) > 0)
                                <div class="alert alert-danger">
                                    <ul>
                                        @foreach ($errors->all() as $error)
                                            <li>{{ $error }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            @endif

                            {!! Form::open(array('route' => 'lives.store','method'=>'POST','files' => true,'class'=>"form-horizontal group-border-dashed")) !!}
                            <div class="form-group has-feedback">
                                <label class="col-sm-3 control-label">タイトル</label>
                                <div class="col-sm-7">
                                    {!! Form::text('title', null, array('placeholder' => 'Title','class' => 'form-control', 'required')) !!}
                                    <span class="glyphicon form-control-feedback"></span>
                                </div>
                            </div>


                            <div class="form-group">
                                <label class="col-sm-3 control-label">サムネイル</label>
                                <div class="col-sm-7">
                                    {!! Form::file('image', null, array('placeholder' => 'jpg,png','class' => 'form-control filestyle', 'data-icon'=>"false",'data-buttonname'=>"btn-white", 'required')) !!}
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label">チャネル名</label>
                                <div class="col-sm-7">
                                    {!! Form::text('channel_name', null, array('placeholder' => 'チャネル名','class' => 'form-control', 'required')) !!}
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-3 control-label">プログラム名</label>
                                <div class="col-sm-7">
                                    {!! Form::text('program_name', null, array('placeholder' => 'プログラム名','class' => 'form-control','required')) !!}
                                </div>
                            </div>

                            <div class="form-group has-feedback">
                                <label class="col-sm-3 control-label">開始時間</label>
                                <div class="col-sm-7">
                                    <div class="input-group" style="width: 200px; float: left">
                                        {!! Form::text('start_date', null, array('class' => 'form-control datepicker', 'required')) !!}
                                        <span class="input-group-addon bg-custom b-0 text-white"><i class="icon-calender"></i></span>
                                    </div>
                                    <div class="input-group" style="width: 150px; float: left;margin-left: 20px">
                                        {!! Form::text('start_time', null, array('class' => 'form-control timepicker', 'required')) !!}
                                        <span class="input-group-addon bg-custom b-0 text-white"><i class="glyphicon glyphicon-time"></i></span>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group has-feedback">
                                <label class="col-sm-3 control-label">終了時間</label>
                                <div class="col-sm-7">
                                    <div class="input-group" style="width: 200px; float: left">
                                        {!! Form::text('end_date', null, array('class' => 'form-control datepicker', 'required')) !!}
                                        <span class="input-group-addon bg-custom b-0 text-white"><i class="icon-calender"></i></span>
                                    </div>
                                    <div class="input-group" style="width: 150px; float: left;margin-left: 20px">
                                        {!! Form::text('end_time', null, array('class' => 'form-control timepicker', 'required')) !!}
                                        <span class="input-group-addon bg-custom b-0 text-white"><i class="glyphicon glyphicon-time"></i></span>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group m-b-0">
                                <div class="col-sm-offset-3 col-sm-9 m-t-15">
                                    <button type="submit" class="btn btn-primary">
                                        保存
                                    </button>
                                </div>
                            </div>
                            {!! Form::close() !!}
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('script')
    <script>
        $(document).ready(function() {
            $('.datepicker').datepicker({
                autoclose: true,
                todayHighlight: true,
                format: 'yyyy/mm/dd'
            });

            $('.timepicker').timepicker({
                showMeridian : false
            })
        })

    </script>
@stop
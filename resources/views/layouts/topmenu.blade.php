<div class="topbar">
    <!-- LOGO -->
    <div class="topbar-left">
        <div class="text-center">
            <a href="{{ route('top') }}" class="logo">
                <i class="icon-c-logo"><img src="{{ asset('images/logo_sm.png') }}" height="42"/></i>
                <span><img src="{{ asset('images/logo_light.png') }}" width="120"/></span>
            </a>
        </div>
    </div>
    <!-- Button mobile view to collapse sidebar menu -->
    <div class="navbar navbar-default" role="navigation">
        <div class="container">
            <div class="pull-left">
                <button class="button-menu-mobile open-left waves-effect waves-light">
                    <i class="md md-menu"></i>
                </button>
                <span class="clearfix"></span>
            </div>
            <form action="{{ route('search') }}" method="get" role="search" class="navbar-left app-search pull-left hidden-xs">
                <input type="text" name="keyword" placeholder="講座/授業を検索する" class="form-control">
                {{--<a href="/search"><i class="fa fa-search"></i></a>--}}
                <button type="submit" style="position: absolute;top: 3px;right: 10px;color: #c4c4cd; border:none; background: transparent"><i class="fa fa-search"></i></button>
            </form>

            @if (Sentinel::check())
                <ul class="nav navbar-nav navbar-right pull-right">
                    <li class="dropdown top-menu-item-xs">
                    </li>
                    <!-- Full Screen -->
                    <li class="hidden-xs">
                        <a href="#" id="btn-fullscreen" class="waves-effect waves-light"><i class="icon-size-fullscreen"></i></a>
                    </li>
                    <!-- Icon Menu -->
                    <li class="dropdown top-menu-item-xs">
                        <a href="" class="dropdown-toggle profile waves-effect waves-light" data-toggle="dropdown" aria-expanded="true">
                            <img src="{{ asset('images/users/avatar-1.jpg') }}" alt="user-img" class="img-circle"> </a>
                        <ul class="dropdown-menu">
                            <li><a href="{{route('profile', ['id' => Sentinel::getUser()->getUserId()])}}"><i class="ti-user m-r-10 text-custom"></i> プロフィール</a></li>
                            <li><a href="{{route('editprofile')}}"><i class="ti-settings m-r-10 text-custom"></i> 個人設定</a></li>
                            <li class="divider"></li>
                            <li><a href="{{route('auth.logout')}}"><i class="ti-power-off m-r-10 text-danger"></i> ログアウト</a></li>
                        </ul>
                    </li>
                    <!-- -->
                </ul>
            @else
                <div class="pull-right">
                    <a href="{{ route('auth.login.form') }}" class="btn btn-pink btn-custom btn-rounded waves-effect waves-light btn-middle">ログイン</a>
                    <a href="{{ route('auth.register.form')}}" class="btn btn-primary btn-custom btn-rounded waves-effect waves-light btn-middle">会員登録</a>
                </div>
            @endif
        </div>
    </div>
</div>





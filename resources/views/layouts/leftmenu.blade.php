<div class="left side-menu">
    <div class="sidebar-inner slimscrollleft">
        <div id="sidebar-menu">
            <ul>
                <li class="text-muted menu-title">Navigation</li>
                <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect"><i class="ti-home"></i> <span> ホーム </span> <span class="menu-arrow"></span></a>
                    <ul class="list-unstyled">
                        <li><a href="{{route('top')}}">トップ</a></li>
                        <li><a href="guide.php">使い方ガイド</a></li>
                        <li><a href="/">ガッコウ＋とは？</a></li>
                    </ul>
                </li>
                <li class="has_sub">
                    <a href="{{ route('courses.index') }}" class="waves-effect"><i class="ti-menu-alt"></i> <span> 講座一覧 </span> <span class="menu-arrow"></span> </a>
                    <ul class="list-unstyled">
                        <!-- <li><a href="live-calendar.php">生放送カレンダー</a></li> -->
                        @php ($categories = \App\Http\Controllers\CourseController::CATEGORIES)
                        @foreach($categories as $no=>$category)
                            <li><a href="{{ route('courses.index', ['category' => $no]) }}">{{ $category }}</a></li>
                        @endforeach
                    </ul>
                </li>
                @if (Sentinel::check())
                <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect"><i class="icon-emotsmile"></i> <span> マイページ </span> <span class="menu-arrow"></span></a>
                    <ul class="list-unstyled" >
                        <li><a href="{{route('profile', ['id' => Sentinel::getUser()->getUserId()])}}"> プロフィール</a></li>
                        <li><a href="{{route('editprofile')}}"> 個人設定</a></li>
                        <li><a href="{{ route('auth.logout') }}">ログアウト</a></li>
                    </ul>
                </li>
                @endif
                <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect"><i class="md-movie"></i> <span> 動画の管理 </span> <span class="menu-arrow"></span></a>
                    <ul class="list-unstyled" >
                        <li class="active"><a href="{{ route('lessons.index') }}" class="active">講座・授業の一覧</a></li>
                        <li><a href="{{ route('lessons.create') }}">授業の投稿</a></li>
                        @if (Sentinel::check())
                            @php($user = Sentinel::getUser())
                            @if($user['service_plan'] == 'premium')
                                <li><a href="{{ route('my_lives') }}">生放送一覧</a></li>
                            @endif
                        @endif
                    </ul>
                </li>
                <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect"><i class="md-note-add"></i> <span> コンテンツの管理 </span> <span class="menu-arrow"></span></a>
                    <ul class="list-unstyled" >
                        <li><a href="#">ブログ</a></li>
                        <li><a href="#">ショップ</a></li>
                    </ul>
                </li>
                <li class="has_sub">
                    <a href="javascript:void(0);" class="waves-effect"><i class="ti-files"></i> <span> その他 </span> <span class="menu-arrow"></span></a>
                    <ul class="list-unstyled">
                        <li><a href="faq.php">よくある質問</a></li>
                        <li><a href="company.php">運営会社</a></li>
                        <li><a href="#">お問い合わせ</a></li>
                    </ul>
                </li>

            </ul>
        </div>
    </div>
</div>